#!/bin/bash

models[0]="LinearSimpleXYA"
models[1]="LinearFullXYA"
models[2]="LinearSimpleHistoryXYA"
models[3]="LinearFullHistoryXYA"

Rscript plot_methods.r test results.csv
mv methods.png avg_rmse.png

Rscript plot_methods.r test tests.csv
mv methods.png avg_error.png

for model in ${models[@]}
do
    Rscript plot_methods.r param ${model}.csv
    mv params.png ${model}.png
done
