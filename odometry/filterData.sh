#!/bin/bash

if [ "$#" -lt 1 ]
then
    echo "Usage: $0 <data_file>"
    exit -1
fi

dataFile=$1

# Create results file
grep RESULTS $dataFile > results.csv && sed -i s/RESULTS,//g results.csv
grep TEST $dataFile > tests.csv && sed -i s/TEST,//g tests.csv

grep PARAMS $dataFile > all_parameters.csv && sed -i s/PARAMS,//g all_parameters.csv

# for linear_xyz model
paramHeader="OdometryModelName,CountLearn,CountTest,Try,SizeParams,"
model[0]="LinearSimpleXYA"
model[1]="LinearFullXYA"
model[2]="LinearSimpleHistoryXYA"
model[3]="LinearFullHistoryXYA"
nbParams[0]=6
nbParams[1]=12
nbParams[2]=9
nbParams[3]=21
#params[0]="offsetX,factX,offsetY,factY,offsetZ,factZ"
#params[1]="offsetX,propXbyX,propXbyY,propXbyZ,offsetY,propYbyX,propYbyY,propYbyZ,offsetZ,propZbyX,propZbyY,propZbyZ"
for modelId in $(seq 0 $((${#model[@]}-1)) )
do
    modelName=${model[$modelId]}
    modelParams=${nbParams[$modelId]}
    output=${modelName}.csv
    printf ${paramHeader} > $output
    for coefId in $(seq 0 $((${modelParams}-2)))
    do
        printf coef%02d, $coefId >> $output
    done
    printf "coef%02d\n" $((${modelParams}-1)) >> $output
    grep ${modelName} all_parameters.csv >> ${modelName}.csv
done
