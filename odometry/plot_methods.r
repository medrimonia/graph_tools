library(ggplot2)
library(stringr)

se <- function(x)
{
    sd(x) / (sqrt(length(x)))
}

ci <- function(x)
{
    1.96 * se(x)
}

getBase <- function(path)
{
    base = str_extract(path,".*/")
    if (is.na(base)) {
        base = "./"
    }
    return(base);
}

getFileName <- function(path)
{
    base <- getBase(path)
    if (base == "./")
    {
        return(path)
    }
    else
    {
        pathLength <- str_length(path)
        baseLength <- str_length(base)
        return(substr(path,baseLength +1,pathLength))
    }
}

getFilePrefix <- function(path)
{
    file <- getFileName(path)
    return(str_extract(file,"[^.]+"))
}

cbbPalette <- c("#000000", "#E69F00", "#56B4E9", "#009E73", "#F0E442", "#0072B2", "#D55E00", "#CC79A7")

methodsPlot <- function(path, enableCI)
{
    data <- read.csv(path)
    testInitMean <- mean(data$InitTest)
    testInitCI <- ci(data$InitTest)
    # Ribbon data for Init CI:
    ribbonInitData <-
        data.frame(CountLearn = c(min(data$CountLearn), max(data$CountLearn)),
                   OdometryModelName = rep("Initial", 2),
                   ymin = rep(testInitMean -testInitCI,2),
                   ymax = rep(testInitMean +testInitCI,2),
                   mean = rep(testInitMean, 2))
    plotData <- do.call(data.frame, aggregate(Test~OdometryModelName + CountLearn,
                                              data,
                                              function(x) c(mean = mean(x),
                                                            ci = ci(x))))
    plotData$mean <- plotData[,"Test.mean"]
    plotData$ymin <- plotData[,"Test.mean"] - plotData[,"Test.ci"]
    plotData$ymax <- plotData[,"Test.mean"] + plotData[,"Test.ci"]
    plotData <- merge(plotData, ribbonInitData, all=TRUE)
    print(plotData)
    # Plotting graph
    g  <- ggplot(plotData,
                 aes(x = CountLearn,
                     y = mean,
                     ymin = ymin,
                     ymax = ymax,
                     group = OdometryModelName,
                     color = OdometryModelName,
                     fill  = OdometryModelName)
                 )
    if (enableCI)
    {
        g <- g + geom_ribbon(size=0,alpha = 0.3)
    }
    print(testInitMean)
    g <- g + geom_point(size=2)
    g <- g + geom_line(size=1)
    ggsave("methods.png",width=16,height=9)
}

parametersPlot <- function(path)
{
    data <- read.csv(path)
    plotData <- data.frame(param = character(),
                           value = double(),
                           ci = double(),
                           CountLearn = integer(),
                           stringsAsFactors=FALSE)
    print(head(data))
    # For each parameter
    for (i in 6:length(names(data)))
    {
        colname <- names(data)[i]
        tmpData <- do.call(data.frame,aggregate(formula(paste0(colname,"~CountLearn")),
                                                data,
                                                function(x) c(mean = mean(x),
                                                              ci = ci(x))))
        nrow <- nrow(tmpData)
        tmpData$param <- rep(colname, nrow)
        tmpData$value <- tmpData[,paste0(colname,".mean")]
        tmpData$ci <- tmpData[,paste0(colname,".ci")]
        tmpData <- tmpData[,c("param","value","ci","CountLearn")]
        plotData <- rbind(plotData,tmpData)
    }
    print(plotData)
    g <- ggplot(plotData, aes_string(x="CountLearn",
                                     y="value",
                                     ymin="value - ci",
                                     ymax="value + ci",
                                     group="param"))
    g <- g + geom_point()
    g <- g + geom_line()
    g <- g + geom_ribbon(alpha=0.3)
    g <- g + facet_wrap(~param, scales="free_y",nrow=3)
    ggsave("params.png",width=16,height=9) 
}


args <- commandArgs(TRUE)

if (length(args) < 2) {
    cat("Usage: ... <type:param|test> <logFiles>\n");
    quit(status=1)
}

type = args[1]
path = args[2]
if (type == "test")
{
    methodsPlot(path, TRUE)
} else if (type == "param") {
    parametersPlot(path)    
} else {
    cat(paste("Unknown type: ", type))
    quit(status=1)
}
