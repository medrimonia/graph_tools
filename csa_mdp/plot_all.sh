#!/bin/bash

Rscript plot_methods.r results.csv
mv methods.png avg_rmse.png

Rscript plot_methods.r tests.csv
mv methods.png avg_error.png
