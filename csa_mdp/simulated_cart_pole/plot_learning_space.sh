#!/bin/bash

logFolder="/home/medrimonia/phd/tmp_data/july22/scp_cart_vs_ang"

Rscript reward_plot.r                                           \
    --cartesian $(find ${logFolder}/cartesian* -name "reward*") \
    --angular   $(find ${logFolder}/angular*   -name "reward*")

Rscript time_plot.r                                           \
    --cartesian $(find ${logFolder}/cartesian* -name "time*") \
    --angular   $(find ${logFolder}/angular*   -name "time*")
