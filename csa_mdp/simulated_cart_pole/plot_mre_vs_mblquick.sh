#!/bin/bash

logFolder="/home/medrimonia/phd/tmp_data/july28/mix_test"

Rscript reward_plot.r                                                     \
    --mre_cart       $(find ${logFolder}/mre_cart*       -name "reward*") \
    --mre_ang        $(find ${logFolder}/mre_ang*        -name "reward*") \
    --mbl_cart_quick $(find ${logFolder}/mbl_cart_quick* -name "reward*") \
    --mbl_ang_quick  $(find ${logFolder}/mbl_ang_quick*  -name "reward*")

