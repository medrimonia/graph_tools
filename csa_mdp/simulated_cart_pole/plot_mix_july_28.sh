#!/bin/bash

logFolder="/home/medrimonia/phd/tmp_data/july28/mix_test"

Rscript reward_plot.r                                                     \
    --mre_cart       $(find ${logFolder}/mre_cart*       -name "reward*") \
    --mre_ang        $(find ${logFolder}/mre_ang*        -name "reward*") \
    --mbl_cart       $(find ${logFolder}/mbl_cart*       -name "reward*") \
    --mbl_ang        $(find ${logFolder}/mbl_ang*        -name "reward*") \
    --mbl_cart_stoch $(find ${logFolder}/mbl_cart_stoch* -name "reward*") \
    --mbl_ang_stoch  $(find ${logFolder}/mbl_ang_stoch*  -name "reward*")

