#!/bin/bash

folder="/home/medrimonia/phd/tmp_data/08_01/long_runs"

Rscript reward_plot.r --no-evolution                                      \
    --cart_stoch $(find $folder -wholename "*cart*stoch*reward_logs.csv") \
    --cart_det   $(find $folder -wholename "*cart*det*reward_logs.csv"  ) \
    --ang_stoch  $(find $folder -wholename "*ang*stoch*reward_logs.csv" ) \
    --ang_det    $(find $folder -wholename "*ang*det*reward_logs.csv"   )

Rscript reward_plot.r                                                        \
    --cart $(find $folder -wholename "*cart*reward_logs.csv" | grep -v eval) \
    --ang  $(find $folder -wholename "*ang*reward_logs.csv"  | grep -v eval)

Rscript plot.r                                                         \
    --cart_stoch $(find $folder -wholename "*cart*stoch*run_logs.csv") \
    --cart_det   $(find $folder -wholename "*cart*det*run_logs.csv"  ) \
    --ang_stoch  $(find $folder -wholename "*ang*stoch*run_logs.csv" ) \
    --ang_det    $(find $folder -wholename "*ang*det*run_logs.csv"   )

