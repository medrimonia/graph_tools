source("plot_tools.r")

args <- commandArgs(TRUE)

if (length(args) < 1) {
    cat("Usage: ... <logFiles>\n");
    quit(status=1)
}

categories <- argsToCategories(args)

data <- NULL

for (catName in names(categories)) {
#    print(paste("Gathering data for", catName))
    for (path in categories[[catName]]) {
#        print(paste("Path: ", path))
        pathData <- read.csv(path)
        head(pathData)
        pathData$category = catName
        pathData$path = path
        if (is.null(data)) {
            data <- pathData
        }
        else {
            data <- rbind(data, pathData)
        }
    }
}

nbIterations <- aggregate(iteration~path+category,data,FUN=max)
print(aggregate(iteration~category,nbIterations,FUN=mean))
