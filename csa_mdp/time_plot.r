source("plot_tools.r")

args <- commandArgs(TRUE)

if (length(args) < 1) {
    cat("Usage: ... <logFiles>\n");
    quit(status=1)
}

categories <- argsToCategories(args)

data <- NULL

print(categories)

for (catName in names(categories)) {
    print(paste("Gathering data for", catName))
    for (path in categories[[catName]]) {
        print(paste("Path: ", path))
        pathData <- read.csv(path)
        head(pathData)
        pathData$category = catName
        pathData$path = path
        if (is.null(data)) {
            data <- pathData
        }
        else {
            data <- rbind(data, pathData)
        }
    }
}

# Description of best scores
bestScores <- aggregate(score~path,data,FUN=max)
print(bestScores[order(bestScores$score),])


g <- ggplot(data, aes(x=elapsed,y=score,group=category,color=category,linetype=category))
#g <- g + geom_point(size=0.02, alpha=0.2)
g <- g + geom_smooth(fill=NA,method="gam", formula = y~s(x,k=100))
g <- g + theme_bw()
g <- g + guides(colour = guide_legend(keywidth = 3, keyheight = 1))
g <- g + xlab("Training time [s]")
g <- g + ylab("Average reward")

#Force axis (for specific situations
#g <- g + scale_y_continuous(limits=c(-150,-75))

ggsave(file="scoreByElapsed.png", g, width = 8, height=4)


# Summary of data
for (catName in names(categories)) {
    catData = data[which(data$category == catName),]
    catMax = max(catData$score)
    path = catData[which(catData$score >= catMax), "path"]
    print(paste("Best for category:",catName))
    print(paste("\tvalue:",catMax))
    print(paste("\tpath:",path))
}
