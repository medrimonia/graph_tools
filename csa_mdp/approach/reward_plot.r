source("../plot_tools.r")

args <- commandArgs(TRUE)

rewardsPlot(args)
for (path in args)
{
    rewardBarPlot(path, 10)
    discRewardBarPlot(path, 10)
}
