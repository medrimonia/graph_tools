source("../plot_tools.r")

args <- commandArgs(TRUE)

if (length(args) < 1) {
    cat("Usage: ... <logFiles>\n");
    quit(status=1)
}

for (i in 1:length(args))
{
    path = args[i]
    timePlot(path)
}
