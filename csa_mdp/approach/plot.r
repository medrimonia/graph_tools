source("../plot_tools.r")

mkCircle <- function(center = c(0,0),diameter = 1, npoints = 100){
    r = diameter / 2
    tt <- seq(0,2*pi,length.out = npoints)
    xx <- center[1] + r * cos(tt)
    yy <- center[2] + r * sin(tt)
    return(data.frame(x = xx, y = yy))
}

mkRotatedRect <- function(center = c(0,0),dx = 1, dy = 1, theta = 0)
{
    xFactor <- c(1,1,-1,-1)
    yFactor <- c(1,-1,-1,1)
    xx <- center[1] + cos(theta) * dx * xFactor - sin(theta) * dy * yFactor
    yy <- center[2] + sin(theta) * dx * xFactor + cos(theta) * dy * yFactor
    return(data.frame(x = xx, y = yy))
}

# center is in robot position
mkRotatedRectFromRobot <- function(robot_x, robot_y, robot_dir,
                                   center, dx, dy)
{
    x <- robot_x + cos(robot_dir) * center[1] - sin(robot_dir) * center[2]
    y <- robot_y + sin(robot_dir) * center[1] + sin(robot_dir) * center[2]
    mkRotatedRect(c(x,y), dx, dy, robot_dir)
}                                  

vectorPlot <- function(data, variables, outputPath)
{
    # Properties
    ballSize <- 0.1 # diameter in [m]
    arrowLength <- 0.05
    arrowWidth <- unit(0.1, "cm")
    kickCenter <- c(0.15, 0)# in robot referential
    kickXTol <- 0.1
    kickYTol <- 0.1
    collisionX <- 0.05
    collisionY <- 0.25
    
    x_var <- "robot_x"
    y_var <- "robot_y"
    # Calculating temporary variables
    vecData <- data
    vecData$cos <- cos(-vecData$target_angle)
    vecData$sin <- sin(-vecData$target_angle)
    vecData$robot_x <- -(vecData$cos * vecData$ball_x - vecData$sin * vecData$ball_y)
    vecData$robot_y <- -(vecData$sin * vecData$ball_x + vecData$cos * vecData$ball_y)
    vecData$end_x <- vecData$robot_x + vecData$cos * arrowLength
    vecData$end_y <- vecData$robot_y + vecData$sin * arrowLength
    # creating a circle
    circleData <- mkCircle(c(0,0),ballSize,npoints = 100)
    # creating target area and forbidden area (only for last step)
    lastEntry <- tail(vecData,1)
    targetData <- mkRotatedRectFromRobot(lastEntry$robot_x, lastEntry$robot_y,
                                         -lastEntry$target_angle, kickCenter, kickXTol, kickYTol)
    collisionData <- mkRotatedRectFromRobot(lastEntry$robot_x, lastEntry$robot_y,
                                            -lastEntry$target_angle, c(0,0), collisionX, collisionY)
    # plotting
    g <- ggplot()
    # plot target area
    g <- g + geom_polygon(aes(x=x,y=y), targetData, size = 0, fill= "violet", alpha=0.5)
    g <- g + geom_polygon(aes(x=x,y=y), collisionData, size = 0, fill= "red", alpha=0.5)
    # plot ball circle
    g <- g + geom_polygon(aes(x=x,y=y), circleData, size = 0, fill= "orange", alpha=0.5)
    # plot vectors for trajectory
    g <- g + geom_segment(aes(x = robot_x,y = robot_y,
                          xend = end_x, yend = end_y,
                          color = step),
                          vecData,
                          arrow = arrow(length = arrowWidth))
    g <- g + scale_x_continuous(breaks = variables[[x_var]][["breaks"]],
                                labels = variables[[x_var]][["labels"]])
    g <- g + scale_y_continuous(breaks = variables[[y_var]][["breaks"]],
                                labels = variables[[y_var]][["labels"]])
    g <- g + coord_cartesian(xlim = variables[[x_var]][["limits"]],
                             ylim = variables[[y_var]][["limits"]])
    ggsave(file = outputPath, g)
}

vectorPlotBests <- function(path, variables, nbRuns = 10)
{
    data <- read.csv(path)
    base <- getBase(path)
    rewards <- aggregate(reward~run, data, sum);
    bestRuns <- rewards[order(-rewards$reward),][seq(1,nbRuns),]$run
    print(rewards[bestRuns,])
    for (rank in seq(1,length(bestRuns)))
    {
        run <- bestRuns[rank]
        filteredData <- data[which(data$run == run),]
        dst <- sprintf("%sbest_vector_plot_%d_run_%d.png", base, rank, run)
        vectorPlot(filteredData, variables, dst)
    }
}

vectorPlotWorsts <- function(path, variables, nbRuns = 10)
{
    data <- read.csv(path)
    base <- getBase(path)
    rewards <- aggregate(reward~run, data, sum)
    worstRunsIdx <- sort(rewards$reward,index.return = TRUE)$ix[seq(1,nbRuns)]
    worstRuns <- rewards[worstRunsIdx,]$run
    print(rewards[worstRunsIdx,])
    for (rank in seq(1,length(worstRuns)))
    {
        run <- worstRuns[rank]
        filteredData <- data[which(data$run == run),]
        dst <- sprintf("%sworst_vector_plot_%d_run_%d.png", base, rank, run)
        vectorPlot(filteredData, variables, dst)
    }
}

vectorPlotLasts <- function(path, variables, nbRuns = 10)
{
    data <- read.csv(path)
    base <- getBase(path)
    rewards <- aggregate(reward~run, data, sum)
    lastRunsIdx <- seq(max(data$run) - nbRuns + 1, max(data$run))
    lastRuns <- rewards[lastRunsIdx,]$run
    print(rewards[lastRunsIdx,])
    for (rank in seq(1,length(lastRuns)))
    {
        run <- lastRuns[rank]
        filteredData <- data[which(data$run == run),]
        dst <- sprintf("%slast_vector_plot_%d_run_%d.png", base, rank, run)
        vectorPlot(filteredData, variables, dst)
    }
}


rewardDensityPlot <- function(path)
{
    data <- read.csv(path)
    base <- getBase(path)
    dst <- sprintf("%sreward_density.png", base)
    rewards <- aggregate(reward~run, data, sum);
    g <- ggplot(rewards, aes(x = run,y = reward))
    g <- g + geom_point(size=0.5)
    g <- g + stat_density2d(aes(color = ..level..), size=3, alpha=0.7, contour=TRUE)
    g <- g + scale_color_gradientn(colors = cbPalette)
    ggsave(dst, width=16, height=9)
}

rewardBarPlot <- function(path, nbGroups = 5)
{
    data <- read.csv(path)
    base <- getBase(path)
    dst <- sprintf("%sreward_barplot.png", base)
    # Computing run rewards
    rewards <- aggregate(reward~run, data, sum);
    nbRuns <- nrow(rewards)
    groupSize <- ceiling(nbRuns / nbGroups)
    ends <- ceiling(rewards$run / groupSize) * groupSize
    starts <- ends - groupSize
    ends[which(ends > nbRuns)] <- nbRuns
    rewards$group <- sprintf("%04d-%04d", starts, ends)
    # Computing mean from group
    groupRewards <- do.call(data.frame, aggregate(reward~group,
                                                  rewards,
                                                  function(x) c(mean = mean(x),
                                                                sd = sd(x),
                                                                se = sd(x) / length(x))))
    print(groupRewards)
    # Plotting means
    g <- ggplot(groupRewards, aes(x=group, y=reward.mean))
    g <- g + geom_point()
    g <- g + geom_errorbar(mapping = aes_string(x= "group",
                                                ymin="reward.mean-reward.se",
                                                ymax="reward.mean+reward.se"))
    ggsave(dst)
}

args <- commandArgs(TRUE)

if (length(args) < 1) {
    cat("Usage: ... <logFiles>\n");
    quit(status=1)
}

# build a list
variables <- list(robot_x      = list(limits = c(-1.5 ,1.5 )),
                  robot_y      = list(limits = c(-1.5 ,1.5 )),
                  target_angle = list(limits = c(-pi  ,pi  )),
                  step_x       = list(limits = c(-0.02,0.04)),
                  step_y       = list(limits = c(-0.03,0.03)),
                  step_theta   = list(limits = c(-0.2 ,0.2 )),
                  d_step_x     = list(limits = c(-0.02,0.02)),
                  d_step_y     = list(limits = c(-0.02,0.02)),
                  d_step_theta = list(limits = c(-0.05,0.05)))

# computing breaks
for (v in names(variables))
{
    min <- variables[[v]][["limits"]][1]
    max <- variables[[v]][["limits"]][2]
    variables[[v]][["breaks"]] = min + (max - min) * seq(0,1,1/4)
}

# computing labels
for (v in names(variables))
{
    breaks <- variables[[v]][["breaks"]]
    variables[[v]][["labels"]] = sapply(breaks, toString)
}

# Overriding labels
variables[["target_angle"]][["labels"]] <- c(expression(-pi),
                                          expression(-pi/2),
                                          0,
                                          expression(pi/2),
                                          expression(pi))


for (i in 1:length(args)) {
    path = args[i]
    #vectorPlotBests(path, variables, 25)
    #vectorPlotWorsts(path, variables, 25)
    vectorPlotLasts(path, variables, 25)
    #rewardDensityPlot(path)
    #rewardBarPlot(path,10)
}
