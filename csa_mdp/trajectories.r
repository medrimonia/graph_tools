library(ggplot2)
library(stringr)

usage <- function() {
    cat("Usage: ... [-s] <logFiles>\n");
    quit(status=1)
}

args <- commandArgs(TRUE)

mode <- "multi_run"
if (args[1] == "-s") {
    mode <- "single_run"
    if (length(args) == 1) {
        usage()
    }
    args <- args[2:length(args)]
}

if (length(args) < 1) {
    usage()
}

for (i in 1:length(args)) {
    path = args[i]
    data <- read.csv(path)
    filePrefix <- str_extract(path,"[^.]+")
    if (max(data$run) > 49){
        data = data[which(data$run > max(data$run) - 49),]
    }
    # printing density_plot
    if (mode == "multi_run"){
        g <- ggplot(data, aes(x=step, y=pos_axis1, group = run))
        g <- g + facet_wrap(~run,nrow = 7)
    }
    if (mode == "single_run"){
        data$step <- seq(1,nrow(data))
        g <- ggplot(data, aes(x=step, y=pos))
    }
#    g <- g + geom_point(aes(y=pos_cart_joint),size = 0.5, color = "red")
    g <- g + geom_point(size = 0.5, color = "blue")
    
    g <- g + scale_x_continuous("steps", limits=c(0,100))
    g <- g + scale_y_continuous("pos [rad]",
                                limits=c(-pi,pi),
                                breaks=c(-pi,0,pi),
                                labels=c(expression(-pi),0,expression(pi)))
    g <- g + scale_color_gradientn(colors = rainbow(2))
    ggsave(paste("trajectory_",filePrefix,".png", sep =""), width = 16, height = 9)
}
