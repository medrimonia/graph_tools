source("../plot_tools.r")

args <- commandArgs(TRUE)

# Checking args numbers
if (length(args) < 1) {
    cat("Usage: ... <logFiles>\n");
    quit(status=1)
}

categories <- argsToCategories(args)


compareCosts(args, "run", costColumn = "reward", nbGroups=1,
             costLogScale = FALSE)
