source("../plot_tools.r")

goalThickness <- 0.2
goalWidth <- 2.6
goalAreaWidth <- 1.0
goalAreaLength <- 5.0
fieldLength <- 9.0


goalData <- data.frame(xmin = fieldLength / 2,
                       xmax = fieldLength / 2 + goalThickness,
                       ymin = -goalWidth / 2,
                       ymax =  goalWidth / 2)
goalAreaData <- data.frame(xmin = fieldLength / 2 - goalAreaWidth,
                           xmax = fieldLength / 2,
                           ymin = -goalAreaLength / 2,
                           ymax =  goalAreaLength / 2)

vectorPlot <- function(data, variables, outputPath)
{
    src_idx <- 1:(nrow(data) - 1)
    dst_idx <- 2:nrow(data)
    # Ball positions
    ballData <- data.frame(ball_x = data[src_idx,"ball_x"],
                           ball_y = data[src_idx,"ball_y"],
                           ball_end_x = data[dst_idx,"ball_x"],
                           ball_end_y = data[dst_idx,"ball_y"],
                           kick_dir = data[src_idx,"kick_direction"],
                           kick_power = data[src_idx,"kick_power"],
                           step = data[src_idx,"step"])
    ballData$endX <- ballData$ball_x + cos(ballData$kick_dir) * ballData$kick_power
    ballData$endY <- ballData$ball_y + sin(ballData$kick_dir) * ballData$kick_power
    kickVecWidth <- unit(0.1, "cm")
    # Robot positions
    playerVecLength <- 0.2
    playerData <- data.frame(robot_x = data[,"robot_x"],
                             robot_y = data[,"robot_y"],
                             robot_dir = data[,"robot_theta"],
                             step = data[,"step"])
    g <- ggplot()
    g <- g + geom_point(data = ballData,
                        mapping = aes(x=ball_x, y=ball_y, color=step))
    # Print all segments
    g <- g + geom_segment(data = ballData,
                          mapping = aes(x=ball_x, y=ball_y,
                                        xend = ball_end_x, yend = ball_end_y,
                                        color=step))
    g <- g + geom_segment(data = ballData,
                          mapping = aes(x=ball_x, y=ball_y, xend = endX, yend = endY),
                          color = "red",
                          arrow = arrow(length = kickVecWidth))
    # plot vectors for trajectory
    g <- g + geom_point(data=playerData, mapping = aes(x = robot_x, y = robot_y, color=step), shape='r', size = 3)
    # plot goal line
    g <- g + geom_rect(data=goalData,
                       mapping = aes(xmin=xmin,xmax=xmax,ymin=ymin,ymax=ymax),
                       fill="green",
                       alpha= 0.5)
    # plot goal area
    g <- g + geom_rect(data=goalAreaData,
                       mapping = aes(xmin=xmin,xmax=xmax,ymin=ymin,ymax=ymax),
                       fill="red",
                       alpha= 0.5)
    g <- g + scale_x_continuous(breaks=seq(-4,4,1))
    g <- g + coord_cartesian(xlim=c(-4.5,4.5),ylim=c(-3,3))
    ggsave(outputPath, width=9,height=6)
}

vectorPlotBests <- function(path, variables, nbRuns = 10)
{
    data <- read.csv(path,row.names=NULL)
    base <- getBase(path)
    rewards <- aggregate(reward~run, data, sum);
    bestRuns <- rewards[order(-rewards$reward),][seq(1,nbRuns),]$run
    print(rewards[bestRuns,])
    for (rank in seq(1,length(bestRuns)))
    {
        run <- bestRuns[rank]
        filteredData <- data[which(data$run == run),]
        dst <- sprintf("%sbest_vector_plot_%d_run_%d.png", base, rank, run)
        vectorPlot(filteredData, variables, dst)
    }
}

vectorPlotWorsts <- function(path, variables, nbRuns = 10)
{
    data <- read.csv(path)
    base <- getBase(path)
    rewards <- aggregate(reward~run, data, sum)
    worstRunsIdx <- sort(rewards$reward,index.return = TRUE)$ix[seq(1,nbRuns)]
    worstRuns <- rewards[worstRunsIdx,]$run
    print(rewards[worstRunsIdx,])
    for (rank in seq(1,length(worstRuns)))
    {
        run <- worstRuns[rank]
        filteredData <- data[which(data$run == run),]
        dst <- sprintf("%sworst_vector_plot_%d_run_%d.png", base, rank, run)
        vectorPlot(filteredData, variables, dst)
    }
}

vectorPlotLasts <- function(path, variables, nbRuns = 10)
{
    data <- read.csv(path)
    base <- getBase(path)
    rewards <- aggregate(reward~run, data, sum)
    lastRunsIdx <- seq(max(data$run) - nbRuns + 1, max(data$run))
    lastRuns <- rewards[lastRunsIdx,]$run
    print(rewards[lastRunsIdx,])
    for (rank in seq(1,length(lastRuns)))
    {
        run <- lastRuns[rank]
        filteredData <- data[which(data$run == run),]
        dst <- sprintf("%slast_vector_plot_%d_run_%d.png", base, rank, run)
        vectorPlot(filteredData, variables, dst)
    }
}

args <- commandArgs(TRUE)

if (length(args) < 1) {
    cat("Usage: ... <logFiles>\n");
    quit(status=1)
}

for (i in 1:length(args)) {
    path = args[i]
    vectorPlotBests(path, variables, 50)
#    vectorPlotWorsts(path, variables, 10)
}
warnings()
