source("../plot_tools.r")

# Global variables

goalThickness <- 0.2
goalWidth <- 2.6
goalAreaWidth <- 1.0
goalAreaLength <- 5.0
fieldLength <- 9.0
goalieX <- fieldLength / 2 - 0.1
goalieY <- 0
goalieThickness <- 0.2
goalieWidth <- 0.4
ballRadius <- 0.07

actions <- c("CustomPower","FullPower", "CustomPower")
full_power <- 3.5 # Default distance traveled by a FullPower Kick
robotLength <- 0.15

# Width of arrows
robotVecWidth <- unit(0.1, "cm")
kickVecWidth <- unit(0.1, "cm")

goalData <- data.frame(xmin = fieldLength / 2,
                       xmax = fieldLength / 2 + goalThickness,
                       ymin = -goalWidth / 2,
                       ymax =  goalWidth / 2)
goalieData <- data.frame(xmin = goalieX - goalieThickness/2 - ballRadius,
                         xmax = goalieX + goalieThickness/2 + ballRadius,
                         ymin = goalieY - goalieWidth/2 - ballRadius,
                         ymax = goalieY + goalieWidth/2 + ballRadius)
goalAreaData <- data.frame(xmin = fieldLength / 2 - goalAreaWidth,
                           xmax = fieldLength / 2,
                           ymin = -goalAreaLength / 2,
                           ymax =  goalAreaLength / 2)

vectorPlot <- function(data, variables, outputPath)
{
    src_idx <- 1:(nrow(data) - 1)
    dst_idx <- 2:nrow(data)
    # Ball positions
    ballData <- data.frame(ball_x = data[src_idx,"ball_x"],
                           ball_y = data[src_idx,"ball_y"],
                           ball_end_x = data[dst_idx,"ball_x"],
                           ball_end_y = data[dst_idx,"ball_y"],
                           step = data[src_idx,"step"])
    # Robot positions
    playerData <- data.frame(robot_x = data[,"robot_x"],
                             robot_y = data[,"robot_y"],
                             robot_dir = data[,"robot_theta"],
                             step = data[,"step"])
    playerData$end_x <- playerData$robot_x + cos(playerData$robot_dir) * robotLength
    playerData$end_y <- playerData$robot_y + sin(playerData$robot_dir) * robotLength
    g <- ggplot()
    # plot goal line
    g <- g + geom_rect(data=goalData,
                       mapping = aes(xmin=xmin,xmax=xmax,ymin=ymin,ymax=ymax),
                       fill="green",
                       alpha= 0.5)
    # plot goal area
    g <- g + geom_rect(data=goalAreaData,
                       mapping = aes(xmin=xmin,xmax=xmax,ymin=ymin,ymax=ymax),
                       fill="gray",
                       alpha= 1.0)
    # plot goalie area
    g <- g + geom_rect(data=goalieData,
                       mapping = aes(xmin=xmin,xmax=xmax,ymin=ymin,ymax=ymax),
                       fill="red",
                       alpha= 0.8)
    # Plot balls
    g <- g + geom_point(data = ballData,
                        mapping = aes(x=ball_x, y=ball_y, color=step))
    # Print all kicks
    for (action_id in seq(0, length(actions)-1)) {
        action_type = actions[action_id+1]#R indices start by 1
        indices <- which(data$action_id == action_id)
        action_data <- data.frame(src_x = data[indices,"ball_x"],
                                  src_y = data[indices,"ball_y"])
        kick_dir_name <- paste("a",action_id,"_kick_direction",sep="")
        action_data$kick_dir <- data[indices,kick_dir_name]
        if (action_type == "CustomPower") {
            kick_pow_name <- paste("a",action_id,"_kick_power",sep="")
            action_data$kick_pow <- data[indices,kick_pow_name]
        }
        else if (action_type == "FullPower"){
            action_data$kick_pow <- rep(full_power, length(indices))
        }
        else {
            print(paste("INVALID ACTION TYPE:", action_type))
        }
        action_data$end_x <- action_data$src_x + cos(action_data$kick_dir) * action_data$kick_pow
        action_data$end_y <- action_data$src_y + sin(action_data$kick_dir) * action_data$kick_pow
        g <- g + geom_segment(data = action_data,
                              mapping = aes(x=src_x, y=src_y, xend = end_x, yend = end_y),
                              color = "brown",
                              linetype = "dashed",
                              arrow = arrow(length = kickVecWidth))
    }
    # Print all balls moves
    g <- g + geom_segment(data = ballData,
                          mapping = aes(x=ball_x, y=ball_y,
                                        xend = ball_end_x, yend = ball_end_y,
                                        color=step))
    # plot vectors for robot positions
    g <- g + geom_segment(data=playerData,
                          mapping = aes(x = robot_x, y = robot_y,
                                        xend = end_x, yend = end_y,
                                        color=step),
                          arrow = arrow(length = robotVecWidth))
    g <- g + scale_x_continuous(breaks=seq(-4,4,1))
    g <- g + coord_cartesian(xlim=c(-4.5,4.5),ylim=c(-3,3))
    g <- g + theme_bw()
    ggsave(outputPath, width=9,height=6)
}

vectorPlotBests <- function(path, variables, nbRuns = 10)
{
    data <- read.csv(path,row.names=NULL)
    base <- getBase(path)
    rewards <- aggregate(reward~run, data, sum);
    bestRuns <- rewards[order(-rewards$reward),][seq(1,nbRuns),]$run
    print(rewards[bestRuns,])
    for (rank in seq(1,length(bestRuns)))
    {
        run <- bestRuns[rank]
        filteredData <- data[which(data$run == run),]
        dst <- sprintf("%sbest_vector_plot_%d_run_%d.png", base, rank, run)
        vectorPlot(filteredData, variables, dst)
    }
}

vectorPlotWorsts <- function(path, variables, nbRuns = 10)
{
    data <- read.csv(path)
    base <- getBase(path)
    rewards <- aggregate(reward~run, data, sum)
    worstRunsIdx <- sort(rewards$reward,index.return = TRUE)$ix[seq(1,nbRuns)]
    worstRuns <- rewards[worstRunsIdx,]$run
    print(rewards[worstRunsIdx,])
    for (rank in seq(1,length(worstRuns)))
    {
        run <- worstRuns[rank]
        filteredData <- data[which(data$run == run),]
        dst <- sprintf("%sworst_vector_plot_%d_run_%d.png", base, rank, run)
        vectorPlot(filteredData, variables, dst)
    }
}

vectorPlotLasts <- function(path, variables, nbRuns = 10)
{
    data <- read.csv(path)
    base <- getBase(path)
    rewards <- aggregate(reward~run, data, sum)
    lastRunsIdx <- seq(max(data$run) - nbRuns + 1, max(data$run))
    lastRuns <- rewards[lastRunsIdx,]$run
    print(rewards[lastRunsIdx,])
    for (rank in seq(1,length(lastRuns)))
    {
        run <- lastRuns[rank]
        filteredData <- data[which(data$run == run),]
        dst <- sprintf("%slast_vector_plot_%d_run_%d.png", base, rank, run)
        vectorPlot(filteredData, variables, dst)
    }
}

args <- commandArgs(TRUE)

if (length(args) < 1) {
    cat("Usage: ... <logFiles>\n");
    quit(status=1)
}

for (i in 1:length(args)) {
    path = args[i]
    vectorPlotLasts(path, variables, 50)
}
warnings()
