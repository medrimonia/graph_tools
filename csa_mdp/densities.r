library(ggplot2)
library(stringr)

args <- commandArgs(TRUE)

if (length(args) < 1) {
    cat("Usage: ... <logFiles>\n");
    quit(status=1)
}
cbPalette <- c("#E69F00", "#56B4E9")

for (i in 1:length(args)) {
    path = args[i]
    data <- read.csv(path)
    filePrefix <- str_extract(path,"[^.]+")
    # printing density_plot
    g <- ggplot(data, aes(x=pos_axis1,y=vel_axis1))
    g <- g + geom_point(size=0.5)
    g <- g + stat_density2d(aes(color = ..level..), size=3, alpha=0.7, contour=TRUE)
    g <- g + scale_color_gradientn(colors = cbPalette)
    g <- g + scale_x_continuous("theta [rad]",
                                limits=c(-pi,pi),
                                breaks=c(-pi,0,pi),
                                labels=c(expression(-pi),0,expression(pi)))
    g <- g + scale_y_continuous("omega [rad/s]",
                                limits=c(-20,20),
                                breaks=c(-20,0,20),
                                labels=c(-20,0,20))
    ggsave(paste("density_", filePrefix,".png", sep =""))
}
