args <- commandArgs(TRUE)

data <- NULL

for (path in args) {
    pathData <- read.csv(path)
    pathData$path = path
    if (is.null(data)) {
        data <- pathData
    }
    else {
        data <- rbind(data, pathData)
    }
}

write.csv(data, file="gathered_evaluations.csv")
