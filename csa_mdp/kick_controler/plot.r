source("../plot_tools.r")

colorGraphs <- TRUE

# CBB palette
cbb <- c("#000000", "#E69F00", "#56B4E9", "#009E73", "#F0E442", "#0072B2", "#D55E00", "#CC79A7")


# Global variables

goalThickness <- 0.2
goalWidth <- 2.6
goalAreaWidth <- 1.0
goalAreaLength <- 5.0
fieldLength <- 8.0
goalieX <- fieldLength / 2 - 0.1
goalieY <- 0
goalieThickness <- 0.2
goalieWidth <- 0.4
ballRadius <- 0.07

# Width of arrows
robotVecWidth <- unit(0.15, "cm")
kickVecWidth <- unit(0.2, "cm")
robotLength <- 0.2# Length of the vector in meters
labelDist <- 0.25

goalData <- data.frame(xmin = fieldLength / 2,
                       xmax = fieldLength / 2 + goalThickness,
                       ymin = -goalWidth / 2,
                       ymax =  goalWidth / 2,
                       zone = "Goal")
goalieData <- data.frame(xmin = goalieX - goalieThickness/2 - ballRadius,
                         xmax = goalieX + goalieThickness/2 + ballRadius,
                         ymin = goalieY - goalieWidth/2 - ballRadius,
                         ymax = goalieY + goalieWidth/2 + ballRadius,
                         zone = "Goalie")
goalAreaData <- data.frame(xmin = fieldLength / 2 - goalAreaWidth,
                           xmax = fieldLength / 2,
                           ymin = -goalAreaLength / 2,
                           ymax =  goalAreaLength / 2)

# NUMBER OF TRAJECTORIES PLOTTED
nbPlots <- 10

# LIST OF ROBOTS:
robots <- c()
robots <- c("p1")
robots <- c("p1","p2")

# LIST OF KICKS:
# Warning: currently we use a list of actions and a list of kick_powers
#          for CustomKicks, an entry with "None" should be added in kick_powers
actions <- c("FinalKick","DirectedKick", "DirectedKick","FinalKick","DirectedKick", "DirectedKick")
kick_powers <- c(3.0,1.5,3.0,3.0,1.5,3.0)

vectorPlot <- function(data, variables, outputPath)
{
    src_idx <- 1:(nrow(data) - 1)
    dst_idx <- 2:nrow(data)
    # Ball positions
    ballData <- data.frame(ball_x = data[src_idx,"ball_x"],
                           ball_y = data[src_idx,"ball_y"],
                           ball_end_x = data[dst_idx,"ball_x"],
                           ball_end_y = data[dst_idx,"ball_y"],
                           step = data[src_idx,"step"])
    g <- ggplot()

    zones <- rbind(goalData, goalieData)
    # plot zones
    g <- g + geom_rect(data=zones,
                       mapping = aes(xmin=xmin,xmax=xmax,ymin=ymin,ymax=ymax,fill=zone),
                       alpha= 1)
    # Plot balls
    g <- g + geom_point(data = ballData,
                        mapping = aes(x=ball_x, y=ball_y, color=step))
    # Print each type of kick separately
    wished_kicks <- NULL
    for (action_id in seq(0, length(actions)-1)) {
        action_type = actions[action_id+1]#R indices start by 1
        indices <- which(data$action_id == action_id)
        action_data <- data.frame(x = data[indices,"ball_x"],
                                  y = data[indices,"ball_y"],
                                  step = data[indices,"step"],
                                  kick_type = rep("wished", length(indices)))
        # Define eventual names used
        kick_dir_name <- paste("a",action_id,"_target_dir",sep="")
        kick_target_y_name <- paste("a",action_id,"_target_y",sep="")
        kick_dirs <- NULL
        kick_pows <- NULL
        if (action_type == "DirectedKick"){
            kick_dirs <- data[indices,kick_dir_name]
            kick_pows <- rep(kick_powers[action_id+1], length(indices))
        }
        else if (action_type == "FinalKick"){
            kick_pows <- rep(kick_powers[action_id+1], length(indices))
            # Compute direction of the kick
            target_y <- data[indices, kick_target_y_name]
            dx <- (fieldLength / 2) - action_data$x
            dy <- target_y - action_data$y
            kick_dirs <- atan2(dy,dx)
        }
        else {
            print(paste("INVALID ACTION TYPE:", action_type))
        }
        action_data$xend <- action_data$x + cos(kick_dirs) * kick_pows
        action_data$yend <- action_data$y + sin(kick_dirs) * kick_pows
#        if (nrow(action_data) > 0) {
#            g <- g + geom_segment(data = action_data,
#                                  mapping = aes(x=x, y=y, xend = xend, yend = yend),
#                                  color = "brown",
#                                  linetype = "dashed",
#                                  arrow = arrow(length = kickVecWidth))
#        }
        if (is.null(wished_kicks)) {
            wished_kicks <- action_data
        } else {
            wished_kicks <- rbind(wished_kicks, action_data)
        }
    }
    # Print all balls moves
    real_kicks <- data.frame(x = ballData$ball_x,
                             y = ballData$ball_y,
                             step = ballData$step,
                             kick_type = rep("real", nrow(ballData)),
                             xend = ballData$ball_end_x,
                             yend = ballData$ball_end_y)

    kicks <- rbind(real_kicks, wished_kicks)
    g <- g + geom_segment(data = kicks,
                          mapping = aes(x=x, y=y,
                                        xend = xend, yend = yend,
                                        color=step, linetype=kick_type),
                          arrow = arrow(length = kickVecWidth))
                             
#    g <- g + geom_segment(data = ballData,
#                          mapping = aes(x=ball_x, y=ball_y,
#                                        xend = ball_end_x, yend = ball_end_y,
#                                        color=step))
    # plot vectors for robot positions
    for (robot in robots) {
        # Robot positions
        playerData <- data.frame(robot_x = data[,paste(robot,"x",sep="_")],
                                 robot_y = data[,paste(robot,"y",sep="_")],
                                 robot_dir = data[,paste(robot,"theta",sep="_")],
                                 step = data[,"step"])
        playerData$end_x <- playerData$robot_x + cos(playerData$robot_dir) * robotLength
        playerData$end_y <- playerData$robot_y + sin(playerData$robot_dir) * robotLength
        playerData$lab_x <- playerData$robot_x - cos(playerData$robot_dir) * labelDist
        playerData$lab_y <- playerData$robot_y - sin(playerData$robot_dir) * labelDist
        g <- g + geom_segment(data=playerData,
                              mapping = aes(x = robot_x, y = robot_y,
                                            xend = end_x, yend = end_y,
                                            color=step),
                              size=1,
                              arrow = arrow(length = robotVecWidth))
        g <- g + geom_text(data=playerData,
                           mapping = aes(x = lab_x, y = lab_y),
                           size=5,
                           label=robot)
    }
    g <- g + scale_x_continuous(breaks=seq(-4,4,1))
    if (colorGraphs) {
        g <- g + scale_color_gradient(low=cbb[4], high=cbb[7])
        g <- g + scale_fill_manual(name="Zones",
                                   values=c("Goal" = cbb[3], "Goalie" = cbb[2]))
    } else {
        g <- g + scale_color_gradient(low="black", high="grey50")
        g <- g + scale_fill_manual(name="Zones",
                                   labels=c("Goal","Goalie"),
                                   values=c("grey90","grey70"))
    }
    g <- g + coord_cartesian(xlim=c(-4,5),ylim=c(-3,3))
    g <- g + xlab("x [m]")
    g <- g + ylab("y [m]")
    g <- g + theme_bw()
    ggsave(outputPath, width=9,height=6)
}

vectorPlotBests <- function(path, variables, nbRuns = 10)
{
    data <- read.csv(path,row.names=NULL)
    base <- getBase(path)
    rewards <- aggregate(reward~run, data, sum);
    bestRuns <- rewards[order(-rewards$reward),][seq(1,nbRuns),]$run
    print(rewards[bestRuns,])
    for (rank in seq(1,length(bestRuns)))
    {
        run <- bestRuns[rank]
        filteredData <- data[which(data$run == run),]
        dst <- sprintf("%sbest_vector_plot_%d_run_%d.png", base, rank, run)
        vectorPlot(filteredData, variables, dst)
    }
}

vectorPlotWorsts <- function(path, variables, nbRuns = 10)
{
    data <- read.csv(path)
    base <- getBase(path)
    rewards <- aggregate(reward~run, data, sum)
    worstRunsIdx <- sort(rewards$reward,index.return = TRUE)$ix[seq(1,nbRuns)]
    worstRuns <- rewards[worstRunsIdx,]$run
    print(rewards[worstRunsIdx,])
    for (rank in seq(1,length(worstRuns)))
    {
        run <- worstRuns[rank]
        filteredData <- data[which(data$run == run),]
        dst <- sprintf("%sworst_vector_plot_%d_run_%d.png", base, rank, run)
        vectorPlot(filteredData, variables, dst)
    }
}

vectorPlotLasts <- function(path, variables, nbRuns = 10)
{
    data <- read.csv(path)
    base <- getBase(path)
    rewards <- aggregate(reward~run, data, sum)
    lastRunsIdx <- seq(max(data$run) - nbRuns + 1, max(data$run))
    lastRuns <- rewards[lastRunsIdx,]$run
    print(rewards[lastRunsIdx,])
    for (rank in seq(1,length(lastRuns)))
    {
        run <- lastRuns[rank]
        filteredData <- data[which(data$run == run),]
        dst <- sprintf("%slast_vector_plot_%d_run_%d.png", base, rank, run)
        vectorPlot(filteredData, variables, dst)
    }
}

args <- commandArgs(TRUE)

if (length(args) < 1) {
    cat("Usage: ... <logFiles>\n");
    quit(status=1)
}

for (i in 1:length(args)) {
    path = args[i]
    vectorPlotLasts(path, variables, nbPlots)
}
warnings()
