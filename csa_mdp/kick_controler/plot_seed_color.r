library(ggplot2)

# CBB palette
cbb <- c("#000000", "#E69F00", "#56B4E9", "#009E73", "#F0E442", "#0072B2", "#D55E00", "#CC79A7")

# Global variables
goalThickness <- 0.2
goalWidth <- 2.6
goalAreaWidth <- 1.0
goalAreaLength <- 5.0
fieldWidth <- 6.0
fieldLength <- 9.0
goalieX <- fieldLength / 2 - 0.1
goalieY <- 0
goalieThickness <- 0.2
goalieWidth <- 0.4
ballRadius <- 0.07

# Width of arrows
robotVecWidth <- unit(0.1, "cm")
kickVecWidth <- unit(0.3, "cm")
robotLength <- 0.15# Length of the vector in meters

# Policy limits
defenseMin <- -fieldLength/2
defenseMax <- 0
fieldYMin <- - fieldWidth/2
fieldYMax <- fieldWidth/2
finishMin <- 2.5
sideYMin <- -goalWidth/2 - 0.3
sideYMax <- goalWidth/2 + 0.3

getRectangleData <- function(xmin,xmax,ymin,ymax,color) {
    data.frame(xmin=xmin,xmax=xmax,ymin=ymin,ymax=ymax,fill=color)
}

getDirectedKick <- function(x,y,power,dirRad) {
    xend <- x + power * cos(dirRad)
    yend <- y + power * sin(dirRad)
    data.frame(x=x,xend=xend,y=y,yend=yend)
}

getFinalKick <- function(x,y,power, yIntercept) {
    dx <- fieldLength/2 - x
    dy <- yIntercept - y
    kickDir <- atan2(dy,dx)
    getDirectedKick(x,y,power,kickDir)
}

areas <- getRectangleData(defenseMin,defenseMax,fieldYMin,fieldYMax,"Defense")
areas <- rbind(areas, getRectangleData(defenseMax,finishMin,sideYMin,sideYMax,"Preparation"))
areas <- rbind(areas, getRectangleData(defenseMax,fieldLength/2,fieldYMin,sideYMin,"Aisle"))
areas <- rbind(areas, getRectangleData(defenseMax,fieldLength/2,sideYMax,fieldYMax,"Aisle"))
areas <- rbind(areas, getRectangleData(finishMin,fieldLength/2,0,sideYMax,"Finish"))
areas <- rbind(areas, getRectangleData(finishMin,fieldLength/2,sideYMin,0,"Finish"))
goalAreas <- getRectangleData(fieldLength/2,fieldLength/2+goalThickness,
                              -goalWidth/2,goalWidth/2,"Goal")
goalAreas <- rbind(goalAreas,
                   getRectangleData(goalieX -goalieThickness/2,goalieX +goalieThickness/2,
                                    goalieY-goalieWidth/2,goalieY+goalieWidth/2,"Goalie"))

# Defense kicks
kicks <- getFinalKick(-3.5,2, 3,0)
kicks <- rbind(kicks, getFinalKick(-2.5,-1.5, 3,0))
# Kicks from aisle
kicks <- rbind(kicks, getDirectedKick(1.0, 2.8, 1.5,-pi * 120 / 180))
kicks <- rbind(kicks, getDirectedKick(3.5, 2, 1.5,-pi * 120 / 180))
kicks <- rbind(kicks, getDirectedKick(1.0, -2.8, 1.5,pi * 120 / 180))
kicks <- rbind(kicks, getDirectedKick(3.5, -2, 1.5,pi * 120 / 180))
# Kicks from the preparation zone
kicks <- rbind(kicks, getDirectedKick(1.2, -0.3, 1.5,0))
kicks <- rbind(kicks, getDirectedKick(0.5, 0.6, 1.5,0))
# Kicks from finisher zone
kicks <- rbind(kicks, getFinalKick(3.0, -0.3, 3.0,-goalWidth/4))
kicks <- rbind(kicks, getFinalKick(3.4, -1.0, 3.0,-goalWidth/4))
kicks <- rbind(kicks, getFinalKick(2.8, 0.5, 3.0,goalWidth/4))
kicks <- rbind(kicks, getFinalKick(3.0, 1.0, 3.0,goalWidth/4))

g <- ggplot()
g <- g + geom_rect(data=areas, mapping=aes(xmin=xmin,xmax=xmax,ymin=ymin,ymax=ymax,fill=fill),
                   color="black",size=0.5)
g <- g + geom_rect(data=goalAreas, mapping=aes(xmin=xmin,xmax=xmax,ymin=ymin,ymax=ymax,fill=fill), size=0)
g <- g + geom_segment(data = kicks,
                      mapping = aes(x=x,xend=xend,y=y,yend=yend),
                      arrow = arrow(length =kickVecWidth))

colorMap <- c("Goal" = cbb[3],
              "Goalie" = cbb[2],
              "Defense" = cbb[7],
              "Preparation" = cbb[5],
              "Aisle" = cbb[6],
              "Finish" = cbb[4]);
g <- g + scale_fill_manual(name="Zones",
                           values= colorMap)
g <- g + coord_fixed(xlim=c(-fieldLength/2,fieldLength/2 + 1.5),ylim=c(fieldYMin,fieldYMax),ratio=1)
g <- g + theme_bw()
ggsave("seed_color.png",width=6,height=3)
