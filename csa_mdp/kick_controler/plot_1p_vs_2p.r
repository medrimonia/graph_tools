library(ggplot2)
# appearance
p1Length <- 0.2
p1Width <- unit(0.1,"cm")
ballSize <- 2
vecLength <- 0.2

args <- commandArgs(TRUE)

if (length(args) != 6) {
    cat("Usage: ... <logFile> <ballX> <ballY> <p1x> <p1y> <p1theta>\n");
    quit(status=1)
}

# parameters
options(digits=10)
ballx <- as.double(args[2])
bally <- as.double(args[3])
p1x <- as.double(args[4])
p1y <- as.double(args[5])
p1dir <- as.double(args[6])

data <- read.csv(args[1])

#data$reward -> p2reward
data$cost1 <- -(data$reward - data$gain)
data$cost2 <- -data$reward
data$percentGain <- 100 * (data$cost1 - data$cost2) / data$cost1

data$xend <- data$x + vecLength * cos(data$dir)
data$yend <- data$y + vecLength * sin(data$dir)


p1data <- data.frame(x=p1x,y=p1y,
                     xend=p1x +cos(p1dir) * p1Length,
                     yend=p1y +sin(p1dir) * p1Length)

ballData <- data.frame(x=ballx, y=bally)


g <- ggplot(data)
# print p2 states colored
g <- g + geom_segment(mapping=aes(x=x,y=y,xend=xend,yend=yend,color=percentGain))
# print p1 pos
g <- g + geom_segment(data=p1data,mapping=aes(x=x,y=y,xend=xend,yend=yend),
                      arrow = arrow(length = p1Width))
# print ball
g <- g + geom_point(data=ballData,mapping=aes(x=x,y=y),size=ballSize)

cbb <- c("#000000", "#E69F00", "#56B4E9", "#009E73", "#F0E442", "#0072B2", "#D55E00", "#CC79A7")

g <- g + coord_fixed(ratio=1,xlim=c(-4,4),ylim=c(-3,3))
g <- g + scale_color_gradientn(colours= c(cbb[2],cbb[3],cbb[4]),
                               values=c(0,0.5,1),
                               limits=c(-60,60),
                               na.value="black")
#g <- g + scale_color_gradient2(low="red",mid="black",high="blue",midpoint=0)
ggsave("test.png",width=4,height=2.5)
