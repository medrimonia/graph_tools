library(ggplot2)
# appearance
pLength <- 0.1
pWidth <- unit(0.1,"cm")
ballSize <- 4
kickWidth <- unit(0.2,"cm")
kickDirLength <- 0.75

# parameters
options(digits=10)
px <- -0.5
py <- 0.5
pdir <- -1.0

pData <- data.frame(x=c(px,-0.3), y=c(py,0), dir=c(pdir,0))
pData$xend <- pData$x +cos(pData$dir) * pLength
pData$yend <- pData$y +sin(pData$dir) * pLength
kickWishedData <- data.frame(x=0, y=0,
                             xend= kickDirLength,
                             yend= 0)

textData <- data.frame(x=c(px,0,kickDirLength),
                       y=c(py,0,0),
                       label=c("robot","ball","kick target"))
# Adding points toward target
nbPoints <- 8
for (i in 2:(nbPoints-1)){
    ratio <- i / nbPoints
    pathPoint <- data.frame(x= px * (1-ratio) -0.3 * ratio,
                            y= py * (1-ratio) - 0.1,
                            label="?")
    textData <- rbind(textData,pathPoint)
}

ballData <- data.frame(x=0, y=0)


g <- ggplot()
# print vectors
g <- g + geom_segment(data=pData,mapping=aes(x=x,y=y,xend=xend,yend=yend),
                      arrow = arrow(length = pWidth))
g <- g + geom_segment(data=kickWishedData,mapping=aes(x=x,y=y,xend=xend,yend=yend),
                      arrow = arrow(length = kickWidth))
# print ball
g <- g + geom_point(data=ballData,mapping=aes(x=x,y=y),size=ballSize)

# Print names
g <- g + geom_text(data=textData,
                   mapping = aes(x = x, y = y + 0.1, label = label),
                   size=3)

cbb <- c("#000000", "#E69F00", "#56B4E9", "#009E73", "#F0E442", "#0072B2", "#D55E00", "#CC79A7")

g <- g + coord_fixed(ratio=1,xlim=c(-1,1),ylim=c(-0.2,0.8))
g <- g + theme_void()
g <- g + theme(panel.background = element_rect(fill = "transparent"))
#g <- g + scale_color_gradient2(low="red",mid="black",high="blue",midpoint=0)
ggsave("leitmotiv_ball_approach.png",width=4,height=2,bg="transparent")
