library(ggplot2)


getRectangleData <- function(xmin,xmax,ymin,ymax,color) {
    data.frame(xmin=xmin,xmax=xmax,ymin=ymin,ymax=ymax,fill=color)
}

# CBB palette
cbb <- c("#000000", "#E69F00", "#56B4E9", "#009E73", "#F0E442", "#0072B2", "#D55E00", "#CC79A7")

# Global variables
goalThickness <- 0.2
goalWidth <- 2.6
goalAreaWidth <- 1.0
goalAreaLength <- 5.0
fieldWidth <- 6.0
fieldLength <- 9.0
goalieX <- fieldLength / 2 - 0.1
goalieY <- 0
goalieThickness <- 0.2
goalieWidth <- 0.4
ballRadius <- 0.07

# Width of arrows
robotVecWidth <- unit(0.1, "cm")
kickVecWidth <- unit(0.3, "cm")
robotLength <- 0.25# Length of the vector in meters


# PARAMETERS

kickStart  <- c(0,-1)
kickDir    <- pi/6
kickLength <- 3

authorizedThetas <- c(kickDir - pi/2, kickDir +pi/2)#, kickDir + pi)
vecLength <- 0.3
dist2Ball <- 0.5

p1Dist <- 0.3
p1 <- c(kickStart[1] - cos(kickDir) * p1Dist, kickStart[2] - sin(kickDir) * p1Dist, kickDir)
p2 <- c(2.5,2,-pi/2)

arrowWidth <- unit(0.2, "cm")

# 1: building kickData
data <- data.frame(x=kickStart[1],
                   y=kickStart[2],
                   dir=kickDir,
                   length=kickLength,
                   type="Expected kick")
kickFinal <- kickStart + c(cos(kickDir),sin(kickDir)) * kickLength

# 2: building targets
for (theta in authorizedThetas) {
    targetData <- data.frame(x=kickFinal[1] + cos(theta + pi) * dist2Ball,
                             y=kickFinal[2] + sin(theta + pi) * dist2Ball,
                             dir=theta,
                             length=vecLength,
                             type="Virtual target")
    data <- rbind(data, targetData)
}

# 3: Adding player positions
for (p in list(p1,p2)) {
    targetData <- data.frame(x=p[1],y=p[2], dir=p[3],
                             length=robotLength,
                             type="Robot")
    data <- rbind(data, targetData)
}

print(data)

# 4: computing end of arrows
data$xend <- data$x + cos(data$dir) * data$length
data$yend <- data$y + sin(data$dir) * data$length

# 5 : building field
goalAreas <- getRectangleData(fieldLength/2,fieldLength/2+goalThickness,
                              -goalWidth/2,goalWidth/2,"Goal")
goalAreas <- rbind(goalAreas,
                   getRectangleData(goalieX -goalieThickness/2,goalieX +goalieThickness/2,
                                    goalieY-goalieWidth/2,goalieY+goalieWidth/2,"Goalie"))

# 6 : drawing

g <- ggplot()
g <- g + geom_segment(data=data,
                      aes(x=x,xend=xend,y=y,yend=yend,linetype=type,size=type),
                      arrow = arrow(length = arrowWidth))
g <- g + geom_rect(data=goalAreas,
                   mapping=aes(xmin=xmin,xmax=xmax,ymin=ymin,ymax=ymax,fill=fill),
                   size=0)

#g <- g +
colorMap <- c("Goal" = cbb[3],
              "Goalie" = cbb[2])
g <- g + scale_fill_manual(name="Zones",
                           values= colorMap)

linetypes <- c("Virtual target" = "dashed",
               "Expected kick" = "solid",
               "Robot" = "solid")
g <- g + scale_linetype_manual(values = linetypes)
lineSizes <- c("Virtual target" = 0.5,
               "Expected kick" = 0.5,
               "Robot" = 1)
g <- g + scale_size_manual(values = lineSizes)
    
g <- g + theme_void()
g <- g + coord_fixed(xlim=c(-2,5),ylim=c(-1.6,2.2), ratio=1)

ggsave("virtual_targets.png",width=6.5,height=3)
