library(ggplot2)

#CBB palette
cbb <- c("#000000", "#E69F00", "#56B4E9", "#009E73", "#F0E442", "#0072B2", "#D55E00", "#CC79A7")

# appearance
pLength <- 0.2
pWidth <- unit(0.1,"cm")
ballSize <- 4
kickWidth <- unit(0.2,"cm")
kickDirLength <- 0.75


# Global variables

goalThickness <- 0.2
goalWidth <- 2.6
goalAreaWidth <- 1.0
goalAreaLength <- 5.0
fieldLength <- 8.0
goalieX <- fieldLength / 2 - 0.1
goalieY <- 0
goalieThickness <- 0.2
goalieWidth <- 0.4
ballRadius <- 0.07

# parameters
options(digits=10)
p1x <- 2
p1y <- 1.5
p1dir <- -1.0
p2x <- 1.5
p2y <- -2
p2dir <- 0.8

ballX <- 3
ballY <- -2.5

kicks <- data.frame(power=c(1.5,1.5,3.0,3.0),
                    dir=c(1.5,0.6,1.2,2.2))
kicks$x <- ballX
kicks$y <- ballY
kicks$xend <- kicks$x + kicks$power * cos(kicks$dir)
kicks$yend <- kicks$y + kicks$power * sin(kicks$dir)



pData <- data.frame(x=c(p1x,p2x), y=c(p1y,p2y), dir=c(p1dir,p2dir))
pData$xend <- pData$x +cos(pData$dir) * pLength
pData$yend <- pData$y +sin(pData$dir) * pLength



goalData <- data.frame(xmin = fieldLength / 2,
                       xmax = fieldLength / 2 + goalThickness,
                       ymin = -goalWidth / 2,
                       ymax =  goalWidth / 2,
                       zone = "goal")
goalieData <- data.frame(xmin = goalieX - goalieThickness/2 - ballRadius,
                         xmax = goalieX + goalieThickness/2 + ballRadius,
                         ymin = goalieY - goalieWidth/2 - ballRadius,
                         ymax = goalieY + goalieWidth/2 + ballRadius,
                         zone = "goalkeeper")
zones <- rbind(goalData, goalieData)

textData <- data.frame(x=c(p1x,p2x,ballX),
                       y=c(p1y + 0.2,p2y -0.2,ballY - 0.3),
                       label=c("p1","p2","ball"))
# Adding points from p1 to target
nbPoints1 <- 8
for (i in 1:(nbPoints1-1)){
    ratio <- i / nbPoints1
    pathPoint <- data.frame(x= p1x * (1-ratio) + ballX * ratio,
                            y= p1y * (1-ratio) + ballY * ratio,
                            label="?")
    textData <- rbind(textData,pathPoint)
}
# Adding points from p2 to target
nbPoints2 <- 5
for (i in 1:(nbPoints2-1)){
    ratio <- i / nbPoints2
    pathPoint <- data.frame(x= p2x * (1-ratio) + ballX * ratio,
                            y= p2y * (1-ratio) + ballY * ratio,
                            label="?")
    textData <- rbind(textData,pathPoint)
}

# Adding points for kicks
kickLabels <- data.frame(x= kicks$x + (kicks$power + 0.2) * cos(kicks$dir),
                         y= kicks$y + (kicks$power + 0.2) * sin(kicks$dir),
                         label="?")
textData <- rbind(textData,kickLabels)

ballData <- data.frame(x=ballX, y=ballY)


g <- ggplot()
# plot zones
g <- g + geom_rect(data=zones,
                   mapping = aes(xmin=xmin,xmax=xmax,ymin=ymin,ymax=ymax,fill=zone),
                   alpha= 1)


# print players
g <- g + geom_segment(data=pData,mapping=aes(x=x,y=y,xend=xend,yend=yend),
                      arrow = arrow(length = pWidth))
g <- g + geom_segment(data=kicks,mapping=aes(x=x,y=y,xend=xend,yend=yend),
                      arrow = arrow(length = kickWidth))
# print ball
g <- g + geom_point(data=ballData,mapping=aes(x=x,y=y),size=ballSize)



# Print names
g <- g + geom_text(data=textData,
                   mapping = aes(x = x, y = y, label = label),
                   size=3)


g <- g + coord_fixed(ratio=1,xlim=c(0.3,4.8),ylim=c(-2.9,1.6))
g <- g + theme_void() + theme(legend.position="none")
g <- g + theme(panel.background = element_rect(fill = "transparent"))
g <- g + scale_fill_manual(name="Zones",
                           labels=c("goal","goalkeeper"),
                           values=c(cbb[3],cbb[2]))
#g <- g + scale_color_gradient2(low="red",mid="black",high="blue",midpoint=0)
ggsave("leitmotiv_kicker_robot.png",width=3,height=3, bg="transparent")
