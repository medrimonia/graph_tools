source("../plot_tools.r")

#CBB palette
cbb <- c("#000000", "#E69F00", "#56B4E9", "#009E73", "#F0E442", "#0072B2", "#D55E00", "#CC79A7")

# Global variables

goalThickness <- 0.2
goalWidth <- 2.6
goalAreaWidth <- 1.0
goalAreaLength <- 5.0
fieldLength <- 9.0
goalieX <- fieldLength / 2 - 0.1
goalieY <- 0
goalieThickness <- 0.2
goalieWidth <- 0.4
ballRadius <- 0.07

# Width of arrows
robotVecWidth <- unit(0.1, "cm")
kickVecWidth <- unit(0.2, "cm")
robotLength <- 0.3# Length of the vector in meters

goalData <- data.frame(xmin = fieldLength / 2,
                       xmax = fieldLength / 2 + goalThickness,
                       ymin = -goalWidth / 2,
                       ymax =  goalWidth / 2,
                       zone = "goal")
goalieData <- data.frame(xmin = goalieX - goalieThickness/2 - ballRadius,
                         xmax = goalieX + goalieThickness/2 + ballRadius,
                         ymin = goalieY - goalieWidth/2 - ballRadius,
                         ymax = goalieY + goalieWidth/2 + ballRadius,
                         zone = "goalkeeper")

# KICKS DATA

xStart <- c(-3,0,2,1.5,3.8)
yStart <- c(1,-1.5,-2,1,3)
yTargets <- c(-1,-1,-1,1,1)

kickPower <- 3
kickLabelDist <- 0.2

# COMPUTING
zones <- rbind(goalData, goalieData)


kicks <- data.frame(x=xStart,y=yStart,yTarget=yTargets)
kicks$dir <- atan2(kicks$yTarget - kicks$y,fieldLength/2 - kicks$x)
kicks$xend <- kicks$x + cos(kicks$dir) * kickPower
kicks$yend <- kicks$y + sin(kicks$dir) * kickPower
kicks$xlab <- (kicks$x + kicks$xend)/2 - sin(kicks$dir) * kickLabelDist
kicks$ylab <- (kicks$y + kicks$yend)/2 + cos(kicks$dir) * kickLabelDist
kicks$label <- paste0("Finisher(",paste0(kicks$yTarget,")"))

# DRAWING

g <- ggplot()

# plot zones
g <- g + geom_rect(data=zones,
                   mapping = aes(xmin=xmin,xmax=xmax,ymin=ymin,ymax=ymax,fill=zone),
                   alpha= 1)

g <- g + geom_segment(data = kicks,
                      mapping = aes(x=x, y=y,
                                    xend = xend, yend = yend),
                      arrow = arrow(length = kickVecWidth))
g <- g + geom_text(data = kicks,
                   mapping = aes(x=xlab,y=ylab,label=label,angle=dir*180/pi),
                   size=3)

g <- g + scale_x_continuous(breaks=seq(-4.5,4.5,1))
g <- g + scale_color_gradient(low="black", high="grey50")
g <- g + scale_fill_manual(name="Zones",
                           labels=c("goal","goalkeeper"),
                           values=c(cbb[3],cbb[2]))
g <- g + coord_fixed(xlim=c(-4,5),ylim=c(-3,3),ratio=1)
g <- g + xlab("x [m]")
g <- g + ylab("y [m]")
g <- g + theme_bw()

ggsave("finisher_kick_visual.png", width=6,height=3)
