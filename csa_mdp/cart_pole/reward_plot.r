source("../plot_tools.r")

args <- commandArgs(TRUE)

if (length(args) < 1) {
    cat("Usage: ... <logFiles>\n");
    quit(status=1)
}

rewardsPlot(args)#,list(limits=c(-35,-15)))
for (path in args)
{
    rewardBarPlot(path, 10)
}
