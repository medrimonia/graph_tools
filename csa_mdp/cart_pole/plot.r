source("../plot_tools.r")

args <- commandArgs(TRUE)

if (length(args) < 1) {
    cat("Usage: ... <logFiles>\n");
    quit(status=1)
}

# build a list
variables <- list(theta      = list(limits = c(-pi,pi)),
                  omega      = list(limits = c(-20,20)),
                  cart_pos   = list(limits = c(-1,1)),
                  cart_speed = list(limits = c(-5,5)),
                  cart_cmd   = list(limits = c(-20,20)))

# computing breaks
for (v in names(variables))
{
    min <- variables[[v]][["limits"]][1]
    max <- variables[[v]][["limits"]][2]
    variables[[v]][["breaks"]] = min + (max - min) * seq(0,1,1/4)
}

# computing labels
for (v in names(variables))
{
    breaks <- variables[[v]][["breaks"]]
    variables[[v]][["labels"]] = sapply(breaks, toString)
}

# Overriding labels
variables[["theta"]][["labels"]] <- c(expression(-pi),
                                      expression(-pi/2),
                                      0,
                                      expression(pi/2),
                                      expression(pi))


for (i in 1:length(args)) {
    path = args[i]
#    heatMap(path, variables[c("pos_cart_joint","vel_cart_joint")])
#    heatMap(path, variables[c("pos_axis1","vel_axis1")])
#    heatMapFacet(path, variables[c("pos_cart_joint","vel_cart_joint")],8,2)
#    heatMapFacet(path, variables[c("theta","omega")],8,2)
#    runsSplitPlot(path, variables, 5)
    plotBestRuns(path, variables, 10)
#    densityPlot(path, variables[c("pos_cart_joint","vel_cart_joint")])
#    densityPlot(path, variables[c("pos_axis1","vel_axis1")])
#    densityFacetPlot(path, variables[c("pos_cart_joint","vel_cart_joint")],8,2)
#    densityFacetPlot(path, variables[c("pos_axis1","vel_axis1")],8,2)
#    runsPlot(path, variables["pos_cart_joint"])
#    runsPlot(path, variables["vel_cart_joint"])
#    runsPlot(path, variables["pos_axis1"     ])
#    runsPlot(path, variables["vel_axis1"     ])
}
