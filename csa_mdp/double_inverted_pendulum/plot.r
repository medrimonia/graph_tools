source("../plot_tools.r")

args <- commandArgs(TRUE)

if (length(args) < 1) {
    cat("Usage: ... <logFiles>\n");
    quit(status=1)
}

# build a list
variables <- list(pos_axis1 = list(limits = c(-pi,pi)),
                  vel_axis1 = list(limits = c(-20,20)),
                  pos_axis2 = list(limits = c(-pi,pi)),
                  vel_axis2 = list(limits = c(-40,40)),
                  cmd_axis1 = list(limits = c(-2,2)),
                  cmd_axis2 = list(limits = c(-1,1)))

# computing breaks
for (v in names(variables))
{
    min <- variables[[v]][["limits"]][1]
    max <- variables[[v]][["limits"]][2]
    variables[[v]][["breaks"]] = min + (max - min) * seq(0,1,1/4)
}

# computing labels
for (v in names(variables))
{
    breaks <- variables[[v]][["breaks"]]
    variables[[v]][["labels"]] = sapply(breaks, toString)
}

# Overriding labels
for (v in c("pos_axis1","pos_axis2"))
{
    variables[[v]][["labels"]] <- c(expression(-pi),
                                    expression(-pi/2),
                                    0,
                                    expression(pi/2),
                                    expression(pi))
}



for (i in 1:length(args)) {
    path = args[i]
    heatMap(path, variables[c("pos_axis1","pos_axis2")])
    heatMapFacet(path, variables[c("pos_axis1","pos_axis2")],8,2)
    runsSplitPlot(path, variables, 5, 4500)
}
