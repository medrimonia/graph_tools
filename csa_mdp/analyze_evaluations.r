args <- commandArgs(TRUE)


rewards <- NULL
for (path in args) {
#    print(paste("# Analyzing ", path))
    data <- read.csv(path)
#    print(paste("mean reward: ", mean(data$reward)))
#    print(paste("#entries: ", nrow(data)))
    newRewards <- data.frame(reward=mean(data$reward), path=path)
    if (is.null(rewards)) {
        rewards <- newRewards
    } else {
        rewards <- rbind(rewards, newRewards)
    }
}
avgPerfs <- aggregate(reward~path, rewards, mean)
avgPerfs <- avgPerfs[order(avgPerfs$reward),]
print("best:")
print(tail(avgPerfs,n=1))
