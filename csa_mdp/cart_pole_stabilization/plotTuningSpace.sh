#!/bin/bash

logFolder13="/home/medrimonia/ownCloud/data/csa_mdp/cart_pole_stabilization/2016_07_13/tuning_space"
logFolder14="/home/medrimonia/phd/tmp_data/july14/tuning_space"

Rscript reward_plot.r                                                           \
    --log_100it_5tr    $(find ${logFolder13}/log*              -name "reward*") \
    --normal_100it_tr  $(find ${logFolder13}/normal*           -name "reward*") \
    --log_50it_2tr     $(find ${logFolder14}/log_50it_2tr*     -name "reward*") \
    --normal_50it_2tr  $(find ${logFolder14}/normal_50it_2tr*  -name "reward*") \
    --log_200it_2tr    $(find ${logFolder14}/log_200it_2tr*    -name "reward*") \
    --normal_200it_2tr $(find ${logFolder14}/normal_200it_2tr* -name "reward*") \
    --log_20it_2tr     $(find ${logFolder14}/log_20it_2tr*     -name "reward*") \
    --normal_20it_2tr  $(find ${logFolder14}/normal_20it_2tr*  -name "reward*")

Rscript time_plot.r                                                           \
    --log_100it_5tr    $(find ${logFolder13}/log*              -name "time*") \
    --normal_100it_tr  $(find ${logFolder13}/normal*           -name "time*") \
    --log_50it_2tr     $(find ${logFolder14}/log_50it_2tr*     -name "time*") \
    --normal_50it_2tr  $(find ${logFolder14}/normal_50it_2tr*  -name "time*") \
    --log_200it_2tr    $(find ${logFolder14}/log_200it_2tr*    -name "time*") \
    --normal_200it_2tr $(find ${logFolder14}/normal_200it_2tr* -name "time*") \
    --log_20it_2tr     $(find ${logFolder14}/log_20it_2tr*     -name "time*") \
    --normal_20it_2tr  $(find ${logFolder14}/normal_20it_2tr*  -name "time*")
