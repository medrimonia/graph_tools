#!/bin/bash

logFolder="/home/medrimonia/phd/tmp_data/july13"

Rscript reward_plot.r --alternative $(find ${logFolder}/alternative* -name "reward_logs.csv") --disabled $(find ${logFolder}/disabled* -name "reward_logs.csv") --mre $(find ${logFolder}/mre* -name "reward_logs.csv")
