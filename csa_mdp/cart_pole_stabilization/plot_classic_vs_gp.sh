#!/bin/bash

logFolder="/home/medrimonia/phd/tmp_data/july20/pwl_vs_gp"

Rscript reward_plot.r                                       \
    --classic $(find ${logFolder}/classic* -name "reward*") \
    --gp      $(find ${logFolder}/gp*      -name "reward*")

Rscript time_plot.r                                       \
    --classic $(find ${logFolder}/classic* -name "time*") \
    --gp      $(find ${logFolder}/gp*      -name "time*")
