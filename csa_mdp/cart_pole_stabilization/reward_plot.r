source("../plot_tools.r")

args <- commandArgs(TRUE)

if (length(args) < 1) {
    cat("Usage: ... <logFiles>\n");
    quit(status=1)
}

costGroups=1000

categories <- argsToCategories(args)
# MULTIPLE CATEGORIES
if (length(categories) > 1) {
    for (groupColumn in c("run","policy")) {
        for (cost in c("reward", "disc_reward")) {
            compareCostsByCategories(categories, groupColumn=groupColumn, costColumn=cost,
                                     nbGroups=costGroups, costLogScale = TRUE)
        }
    }
}
# ONLY ONE CATEGORY
if (length(categories) == 1) {
    for (groupColumn in c("run","policy")) {
        for (cost in c("reward", "disc_reward")) {
            compareCosts(args, groupColumn, costColumn = cost, nbGroups=costGroups,
                         costLogScale = FALSE)
        }
    }
    for (path in args)
    {
        rewardBarPlot(path, costGroups, "policy")
        discRewardBarPlot(path, costGroups, "policy")
    }
}

warnings()
