#!/bin/bash

logFolderOld="/home/medrimonia/ownCloud/data/csa_mdp/cart_pole_stabilization/2016_07_14/tuning_space"
logFolderNew="/home/medrimonia/ownCloud/data/csa_mdp/cart_pole_stabilization/2016_07_14/validation_test"

Rscript reward_plot.r                                                          \
    --log_5h_old     $(find ${logFolderOld}/log_50it_2tr*     -name "reward*") \
    --normal_5h_old  $(find ${logFolderOld}/normal_50it_2tr*  -name "reward*") \
    --log_5h_new     $(find ${logFolderNew}/log_5h*           -name "reward*") \
    --normal_5h_new  $(find ${logFolderNew}/normal_5h_50it*   -name "reward*") \
    --log_2h_new     $(find ${logFolderNew}/log_2h*           -name "reward*") \
    --normal_2h_new  $(find ${logFolderNew}/normal_2h*        -name "reward*") \
    --normal_5h_ful  $(find ${logFolderNew}/normal_5h_1000it* -name "reward*")                                    

Rscript time_plot.r                                                          \
    --log_5h_old     $(find ${logFolderOld}/log_50it_2tr*     -name "time*") \
    --normal_5h_old  $(find ${logFolderOld}/normal_50it_2tr*  -name "time*") \
    --log_5h_new     $(find ${logFolderNew}/log_5h*           -name "time*") \
    --normal_5h_new  $(find ${logFolderNew}/normal_5h_50it*   -name "time*") \
    --log_2h_new     $(find ${logFolderNew}/log_2h*           -name "time*") \
    --normal_2h_new  $(find ${logFolderNew}/normal_2h*        -name "time*") \
    --normal_5h_ful  $(find ${logFolderNew}/normal_5h_1000it* -name "time*")
