#!/bin/bash

logFolder="/home/medrimonia/ownCloud/data/csa_mdp/cart_pole_stabilization/2016_07_21/learning_space2"

Rscript reward_plot.r                                           \
    --cartesian $(find ${logFolder}/cartesian* -name "reward*") \
    --angular   $(find ${logFolder}/angular*   -name "reward*")

Rscript time_plot.r                                           \
    --cartesian $(find ${logFolder}/cartesian* -name "time*") \
    --angular   $(find ${logFolder}/angular*   -name "time*")
