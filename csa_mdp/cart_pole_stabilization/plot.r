source("../plot_tools.r")

args <- commandArgs(TRUE)

if (length(args) < 1) {
    cat("Usage: ... <logFiles>\n");
    quit(status=1)
}

# build a list
variables <- list(state_0  = list(limits = c(-pi/2,pi/2)),
                  state_1  = list(limits = c(-2,2)),
                  action_0 = list(limits = c(-50,50)))

# computing breaks
for (v in names(variables))
{
    min <- variables[[v]][["limits"]][1]
    max <- variables[[v]][["limits"]][2]
    variables[[v]][["breaks"]] = min + (max - min) * seq(0,1,1/4)
}

# computing labels
for (v in names(variables))
{
    breaks <- variables[[v]][["breaks"]]
    variables[[v]][["labels"]] = sapply(breaks, toString)
}

# Overriding labels
variables[["state_0"]][["labels"]] <- c(expression(-pi/2),
                                        expression(-pi/4),
                                        0,
                                        expression(pi/4),
                                        expression(pi/2))



for (i in 1:length(args)) {
    path = args[i]
    heatMap(path, variables[c("state_0","state_1")])
    heatMapFacet(path, variables[c("state_0","state_1")],8,2)
    runsSplitPlot(path, variables, 5)
}
