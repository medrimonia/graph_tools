#!/bin/bash

logFolder="/home/medrimonia/phd/tmp_data/july15/rprop"

Rscript reward_plot.r                                             \
    --25it_2tr   $(find ${logFolder}/25it_2tr*   -name "reward*") \
    --50it_2tr   $(find ${logFolder}/50it_2tr*   -name "reward*") \
    --50it_10tr  $(find ${logFolder}/50it_10tr*  -name "reward*") \
    --200it_2tr  $(find ${logFolder}/200it_2tr*  -name "reward*") \
    --200it_10tr $(find ${logFolder}/200it_10tr* -name "reward*")

Rscript time_plot.r                                             \
    --25it_2tr   $(find ${logFolder}/25it_2tr*   -name "time*") \
    --50it_2tr   $(find ${logFolder}/50it_2tr*   -name "time*") \
    --50it_10tr  $(find ${logFolder}/50it_10tr*  -name "time*") \
    --200it_2tr  $(find ${logFolder}/200it_2tr*  -name "time*") \
    --200it_10tr $(find ${logFolder}/200it_10tr* -name "time*")
