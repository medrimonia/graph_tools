#!/bin/bash

logFolder="/home/medrimonia/phd/tmp_data/08_13"

echo "EVOLUTION REWARD PLOT"

Rscript reward_plot.r                                                           \
    --classic $(find ${logFolder} -wholename "*classic*reward*" | grep -v eval) \
    --both    $(find ${logFolder} -wholename "*both*reward*"    | grep -v eval) \
    --val     $(find ${logFolder} -wholename "*val*reward*"     | grep -v eval) \
    --max     $(find ${logFolder} -wholename "*max*reward*"     | grep -v eval)
