#!/bin/bash

logFolder="/home/medrimonia/phd/tmp_data/08_28"

#echo "NO-EVOLUTION REWARD PLOT"
#
#Rscript reward_plot.r --no-evolution                                            \
#    --stoch_mbl       $(find ${logFolder} -wholename "*quick*stoch*reward*"   ) \
#    --det_mbl         $(find ${logFolder} -wholename "*quick*det*reward*"     ) \

echo "EVOLUTION REWARD PLOT"

Rscript reward_plot.r                                                              \
    --mbl       $(find ${logFolder} -wholename "*mbl*reward*"    | grep -v eval) \
    --mre       $(find ${logFolder} -wholename "*mre*reward*"      | grep -v eval)

#echo "RUN PLOT"
#
#Rscript plot.r                                                                  \
#    --stoch_mbl       $(find ${logFolder} -wholename "*quick*stoch*run*csv"   ) \
#    --det_mbl_quick   $(find ${logFolder} -wholename "*mbl[0-9]*det*run*csv"  )
