#!/bin/bash

logFolder="/home/medrimonia/phd/tmp_data/08_02/polar_approach"

echo "NO-EVOLUTION REWARD PLOT"

Rscript reward_plot.r --no-evolution                                            \
    --stoch_mbl       $(find ${logFolder} -wholename "*quick*stoch*reward*"   ) \
    --stoch_mbl_slow  $(find ${logFolder} -wholename "*slow*stoch*reward*"    ) \
    --stoch_mbl_quick $(find ${logFolder} -wholename "*mbl[0-9]*stoch*reward*") \
    --det_mbl         $(find ${logFolder} -wholename "*quick*det*reward*"     ) \
    --det_mbl_slow    $(find ${logFolder} -wholename "*slow*det*reward*"      ) \
    --det_mbl_quick   $(find ${logFolder} -wholename "*mbl[0-9]*det*reward*"  )

echo "EVOLUTION REWARD PLOT"

Rscript reward_plot.r                                                              \
    --mbl       $(find ${logFolder} -wholename "*quick*reward*"    | grep -v eval) \
    --mbl_slow  $(find ${logFolder} -wholename "*slow*reward*"     | grep -v eval) \
    --mbl_quick $(find ${logFolder} -wholename "*mbl[0-9]*reward*" | grep -v eval) \
    --mre       $(find ${logFolder} -wholename "*mre*reward*"      | grep -v eval)

echo "RUN PLOT"

Rscript plot.r                                                                  \
    --stoch_mbl       $(find ${logFolder} -wholename "*quick*stoch*run*csv"   ) \
    --stoch_mbl_slow  $(find ${logFolder} -wholename "*slow*stoch*run*csv"    ) \
    --stoch_mbl_quick $(find ${logFolder} -wholename "*mbl[0-9]*stoch*run*csv") \
    --det_mbl         $(find ${logFolder} -wholename "*quick*det*run*csv"     ) \
    --det_mbl_slow    $(find ${logFolder} -wholename "*slow*det*run*csv"      ) \
    --det_mbl_quick   $(find ${logFolder} -wholename "*mbl[0-9]*det*run*csv"  )
