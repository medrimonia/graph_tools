#!/bin/bash

logFolder="/home/medrimonia/phd/tmp_data/july13/pa_disc"

Rscript reward_plot.r --0.95 $(find ${logFolder}/disc95* -name "reward_logs.csv") --0.98 $(find ${logFolder}/disc98* -name "reward_logs.csv")
