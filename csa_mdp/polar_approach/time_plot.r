source("../plot_tools.r")

args <- commandArgs(TRUE)

if (length(args) < 1) {
    cat("Usage: ... <logFiles>\n");
    quit(status=1)
}

costGroups=50

categories <- argsToCategories(args)
# MULTIPLE CATEGORIES
if (length(categories) > 1) {
    compareTimeByCategories(categories, nbGroups=costGroups, timeLogScale = FALSE)
}
