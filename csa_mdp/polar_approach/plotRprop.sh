#!/bin/bash

logFolder="/home/medrimonia/phd/tmp_data/july15/pa_rprop"

Rscript reward_plot.r                                            \
    --25it_5tr   $(find ${logFolder}/25it_5tr*  -name "reward*") \
    --50it_5tr   $(find ${logFolder}/50it_5tr*  -name "reward*") \
    --50it_10tr  $(find ${logFolder}/50it_10tr* -name "reward*") \
    --100it_5tr  $(find ${logFolder}/100it_5tr* -name "reward*")

Rscript time_plot.r                                            \
    --25it_5tr   $(find ${logFolder}/25it_5tr*  -name "time*") \
    --50it_5tr   $(find ${logFolder}/50it_5tr*  -name "time*") \
    --50it_10tr  $(find ${logFolder}/50it_10tr* -name "time*") \
    --100it_5tr  $(find ${logFolder}/100it_5tr* -name "time*")
