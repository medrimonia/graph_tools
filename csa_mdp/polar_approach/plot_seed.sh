#!/bin/bash

logFolder="/home/medrimonia/phd/tmp_data/08_03/polar_approach"

echo "NO-EVOLUTION REWARD PLOT"

Rscript reward_plot.r --no-evolution                                               \
    --stoch_mbl         $(find ${logFolder} -wholename "*mbl[0-9]*stoch*reward*" ) \
    --stoch_mbl_seeded  $(find ${logFolder} -wholename "*mbl*seed*stoch*reward*" ) \
    --det_mbl           $(find ${logFolder} -wholename "*mbl[0-9]*det*reward*"   ) \
    --det_mbl_seeded    $(find ${logFolder} -wholename "*mbl*seed*det*reward*"   ) \
    --stoch_mre         $(find ${logFolder} -wholename "*mre[0-9]*stoch*reward*" ) \
    --stoch_mre_seeded  $(find ${logFolder} -wholename "*mre*seed*stoch*reward*" ) \
    --det_mre           $(find ${logFolder} -wholename "*mre[0-9]*det*reward*"   ) \
    --det_mre_seeded    $(find ${logFolder} -wholename "*mre*seed*det*reward*"   )

echo "EVOLUTION REWARD PLOT"

Rscript reward_plot.r                                                                \
    --mbl         $(find ${logFolder} -wholename "*mbl[0-9]*reward*" | grep -v eval) \
    --mbl_seeded  $(find ${logFolder} -wholename "*mbl*seed*reward*" | grep -v eval) \
    --mre         $(find ${logFolder} -wholename "*mre[0-9]*reward*" | grep -v eval) \
    --mre_seeded  $(find ${logFolder} -wholename "*mre*seed*reward*" | grep -v eval) \

echo "RUN PLOT"

Rscript plot.r                                                                    \
    --stoch_mbl_seeded $(find ${logFolder} -wholename "*mbl*seed*stoch*run*csv" ) \
    --stoch_mbl        $(find ${logFolder} -wholename "*mbl[0-9]*stoch*run*csv" ) \
    --det_mbl_seeded   $(find ${logFolder} -wholename "*mbl*seed*det*run*csv"   ) \
    --det_mbl          $(find ${logFolder} -wholename "*mbl[0-9]*det*run*csv"   ) \
    --stoch_mre_seeded $(find ${logFolder} -wholename "*mre*seed*stoch*run*csv" ) \
    --stoch_mre        $(find ${logFolder} -wholename "*mre[0-9]*stoch*run*csv" ) \
    --det_mre_seeded   $(find ${logFolder} -wholename "*mre*seed*det*run*csv"   ) \
    --det_mre          $(find ${logFolder} -wholename "*mre[0-9]*det*run*csv"   ) \
