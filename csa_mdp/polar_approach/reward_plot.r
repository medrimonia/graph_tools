source("../plot_tools.r")

args <- commandArgs(TRUE)

# Do we have an interest in evolution of the rewards?
evolution <- TRUE
if (length(args) >= 1 && args[1] == "--no-evolution") {
    evolution <- FALSE
    args <- args[2:length(args)]
}

# Checking args numbers
if (length(args) < 1) {
    cat("Usage: ... <logFiles>\n");
    quit(status=1)
}

# Custom information based on expert_approach
expertMeanCost <- 17.99
expertMeanDiscCost <- 14.87


costGroups=50

categories <- argsToCategories(args)
# MULTIPLE CATEGORIES
if (length(categories) > 1) {
    if (evolution) {
        for (groupColumn in c("policy")) { # Best reward by run has no meaning here
            for (cost in c("reward", "disc_reward")) {
                hlines <- c()
                if (groupColumn == "policy") {
                    if (cost == "reward") {
                        hlines <- c(expertMeanCost,50)
                    }
                    else {
                        hlines <- c(expertMeanDiscCost,50)
                    }
                }
                compareCostsByCategories(categories, groupColumn=groupColumn, costColumn=cost,
                                         nbGroups=costGroups, costLogScale = FALSE,
                                         hlines = hlines)
            }
        }
    } else {
        cmpCostByCat(categories)
    }
}
# ONLY ONE CATEGORY
if (length(categories) == 1) {
    for (groupColumn in c("run","policy")) {
        for (cost in c("reward", "disc_reward")) {
            compareCosts(args, groupColumn, costColumn = cost, nbGroups=costGroups,
                         costLogScale = TRUE)
        }
    }
    for (path in args)
    {
        rewardBarPlot(path, costGroups, "policy")
        discRewardBarPlot(path, costGroups, "policy")
    }
}
