source("../plot_tools.r")

mkCircle <- function(center = c(0,0),diameter = 1, npoints = 100){
    r = diameter / 2
    tt <- seq(0,2*pi,length.out = npoints)
    xx <- center[1] + r * cos(tt)
    yy <- center[2] + r * sin(tt)
    return(data.frame(x = xx, y = yy))
}

mkRotatedRect <- function(center = c(0,0),dx = 1, dy = 1, theta = 0)
{
    xFactor <- c(1,1,-1,-1)
    yFactor <- c(1,-1,-1,1)
    xx <- center[1] + cos(theta) * dx * xFactor - sin(theta) * dy * yFactor
    yy <- center[2] + sin(theta) * dx * xFactor + cos(theta) * dy * yFactor
    return(data.frame(x = xx, y = yy))
}

# center is in robot position
mkRotatedRectFromRobot <- function(robot_x, robot_y, robot_dir,
                                   center, dx, dy)
{
    x <- robot_x + cos(robot_dir) * center[1] - sin(robot_dir) * center[2]
    y <- robot_y + sin(robot_dir) * center[1] + sin(robot_dir) * center[2]
    mkRotatedRect(c(x,y), dx, dy, robot_dir)
}                                  

vectorPlot <- function(data, outputPath)
{
    # Properties
    ballSize <- 0.1 # diameter in [m]
    arrowLength <- 0.05
    arrowWidth <- unit(0.1, "cm")
    kickCenter <- c(0.225, 0)# in robot referential
    kickXTol <- 0.075
    kickYTol <- 0.06
    collisionX <- 0.05
    collisionY <- 0.25
    
    x_var <- "robot_x"
    y_var <- "robot_y"
    # Calculating temporary variables
    vecData <- data
    vecData$cos <- cos(-vecData$targetAngle)
    vecData$sin <- sin(-vecData$targetAngle)
    vecData$ball_x <- cos(vecData$ballDir) * vecData$ballDist
    vecData$ball_y <- sin(vecData$ballDir) * vecData$ballDist
    vecData$robot_x <- -(vecData$cos * vecData$ball_x - vecData$sin * vecData$ball_y)
    vecData$robot_y <- -(vecData$sin * vecData$ball_x + vecData$cos * vecData$ball_y)
    vecData$end_x <- vecData$robot_x + vecData$cos * arrowLength
    vecData$end_y <- vecData$robot_y + vecData$sin * arrowLength
    # creating a circle
    circleData <- mkCircle(c(0,0),ballSize,npoints = 100)
    # creating target area and forbidden area (only for last step)
    lastEntry <- tail(vecData,1)
    targetData <- mkRotatedRectFromRobot(lastEntry$robot_x, lastEntry$robot_y,
                                         -lastEntry$targetAngle, kickCenter, kickXTol, kickYTol)
    collisionData <- mkRotatedRectFromRobot(lastEntry$robot_x, lastEntry$robot_y,
                                            -lastEntry$targetAngle, c(0,0), collisionX, collisionY)
    targetColor    <- "grey50"
    collisionColor <- "grey75"
    ballColor      <- "grey25"
    # plotting
    g <- ggplot()
    # plot target area
    g <- g + geom_polygon(aes(x=x,y=y), targetData   , size = 0, fill= targetColor   , alpha=1)
    g <- g + geom_polygon(aes(x=x,y=y), collisionData, size = 0, fill= collisionColor, alpha=1)
    # plot ball circle
    g <- g + geom_polygon(aes(x=x,y=y), circleData, size = 0, fill= ballColor, alpha=1)
    # plot vectors for trajectory
    g <- g + geom_segment(aes(x = robot_x,y = robot_y,
                              xend = end_x, yend = end_y),
                          vecData,
                          arrow = arrow(length = arrowWidth))
    # Split according to methods and policies
    g <- g + facet_grid(problem ~ policy)
    # Axis styles and all
    g <- g + xlab("x [m]")
    g <- g + ylab("y [m]")
    g <- g + ggtitle("Trajectories of the robot depending on problem and policy")
    g <- g + coord_fixed(ratio=1)
    g <- g + theme_bw()
    ggsave(file = outputPath, g)
}

gatherData <- function(prefix)
{
    data <- data.frame(problem = character(),
                       policy = character(),
                       ballDist = double(),
                       ballDir = double(),
                       targetAngle = double(),
                       stringsAsFactors=FALSE)
    problems <- c("classic","lateral")
    policies <- c("original","mdp","cmaes")
    for (problem in problems)
    {
        for (policy in policies)
        {
            filename <- paste0(paste(problem, policy, sep="_"),".csv")
            path <- paste(prefix, filename, sep="/")
            tmpData <- read.csv(path)
            tmpData <- tmpData[,c("ballDist","ballDir","targetAngle")]
            tmpData$problem <- rep(problem, nrow(tmpData))
            tmpData$policy <- rep(policy, nrow(tmpData))
            data <- rbind(data, tmpData)
        }
    }
    return(data)
}


args <- commandArgs(TRUE)

if (length(args) < 1) {
    cat("Usage: ... <logFiles>\n");
    quit(status=1)
}

data <- gatherData(args[1])
print(data)
vectorPlot(data, "test.png")

#for (i in 1:length(args)) {
#    path = args[i]
#    data = read.csv(path)
#    base = getBase(path)
#    outputPath = paste0(base, paste0(getFilePrefix(path),".png"))
#    vectorPlot(data, outputPath)
#}

warnings()
