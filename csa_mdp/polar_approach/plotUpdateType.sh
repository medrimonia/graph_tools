#!/bin/bash

logFolder="/home/medrimonia/phd/tmp_data/july16/pa_update_type"

Rscript reward_plot.r                                               \
    --alternative $(find ${logFolder}/alternative* -name "reward*") \
    --mre         $(find ${logFolder}/mre*         -name "reward*") \
    --disabled    $(find ${logFolder}/disabled*    -name "reward*")

Rscript time_plot.r                                               \
    --alternative $(find ${logFolder}/alternative* -name "time*") \
    --mre         $(find ${logFolder}/mre*         -name "time*") \
    --disabled    $(find ${logFolder}/disabled*    -name "time*")
