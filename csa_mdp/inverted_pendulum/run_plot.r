library(ggplot2)
source("../plot_tools.r")

breaks <- c(-pi,-pi/2,0,pi/2,pi)
labels <- c(expression(-pi),
            expression(-pi/2),
            0,
            expression(pi/2),
            expression(pi))

args <- commandArgs(TRUE)

if (length(args) != 2) {
    cat("Usage: ... <logFile> <run>\n");
    quit(status=1)
}

path <- args[1]
run <- as.integer(args[2])

data <- read.csv(path)
data <- data[which(data$run == run),]

g <- ggplot(data, aes(x=step, y=pos_axis))
g <- g + geom_point(size = 0.5)
g <- g + scale_y_continuous(breaks = breaks,
                            labels = labels)
g <- g + coord_cartesian(ylim = c(-pi,pi))
#g <- g + theme_classic()
ggsave("run.png", width = 6, height =3)
