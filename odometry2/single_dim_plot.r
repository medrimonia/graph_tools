library(ggplot2)

cbbPalette <- c("#000000", "#E69F00", "#56B4E9", "#009E73", "#F0E442", "#0072B2", "#D55E00", "#CC79A7")

args <- commandArgs(TRUE)

if (length(args) != 2) {
    cat("Usage: <columnUsed (walkOrder[XYZ])> <runFile.csv>\n");
    quit(status=1)
}

colName <- args[1]
path <- args[2]

data <- read.csv(path, sep=" ")

summary <- aggregate(cbind(walkOrderX,walkOrderY,walkOrderZ,targetX,targetY,targetZ) ~ seqId, data, mean)
allData <- data.frame(walkOrder = summary[colName],
                      measuredX = summary$targetX,
                      measuredY = summary$targetY,
                      measuredAngle = summary$targetZ)
print(head(allData))

allData <- allData[order(allData[colName]),]

for (var in c("measuredX", "measuredY", "measuredAngle")) {
    g <- ggplot(allData, aes_string(x=colName, y = var))
    g <- g + geom_point()
    ggsave(paste(colName, paste0(var,".png"),sep="_"))
}

vecLength <- 0.1
vecWidth <- unit(0.1, "cm")

allData$endX <- allData$measuredX + cos(allData$measuredAngle) * vecLength
allData$endY <- allData$measuredY + sin(allData$measuredAngle) * vecLength

amplitude <- max(max(allData$measuredX) - min(allData$measuredX),
                 max(allData$measuredY) - min(allData$measuredY)) + 2 * vecLength

xlimits <- c(mean(allData$measuredX) - amplitude/2, mean(allData$measuredX) + amplitude/2)
ylimits <- c(mean(allData$measuredY) - amplitude/2, mean(allData$measuredY) + amplitude/2)

g <- ggplot(allData,
            aes_string(x="measuredX", y="measuredY", xend="endX",yend ="endY", color=colName))
g <- g + geom_segment(arrow = arrow(length=vecWidth))
g <- g + coord_cartesian(xlim=xlimits,ylim=ylimits)

g <- g + scale_color_gradient2(low=cbbPalette[2], mid=cbbPalette[1], high=cbbPalette[3])
# Specific case for walkOrderX
#g <- g + scale_color_gradientn(colors = cbbPalette[1:5],
#                               values=c(0,0.2,0.5,0.7,1),
#                               breaks=0:10 * 0.01 - 0.02)
g <- g + theme_bw()

ggsave(width=6,height=4,paste0(colName,".png"))
            

print(allData)
