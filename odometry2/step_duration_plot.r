# Takes multiple odometryLogs provided by LogMachine and produce a scatterplot
# x: walkOrder (intensity of the chosen order, provided as an argument)
# y: avgStepDuration
# output file: step_duration_walkOrder[XYZ].png

library(ggplot2)

cbbPalette <- c("#000000", "#E69F00", "#56B4E9", "#009E73", "#F0E442", "#0072B2", "#D55E00", "#CC79A7")

args <- commandArgs(TRUE)

if (length(args) < 2 || length(args) > 3) {
    cat("Usage: <columnUsed (walkOrder[XYZ])> <runFile.csv> [optional: walkFreq]\n");
    quit(status=1)
}

colName <- args[1]
path <- args[2]

walkFreq <- -1;
if (length(args) > 2) {
    walkFreq <- as.double(args[3])
}

data <- read.csv(path, sep=" ")


plotData <- data.frame(avgDuration = double(),
                       walkOrder = double())

for (seq in unique(data$seqId)) {
    totalDuration <- max (data[which(data$seqId == seq),"timestamp"])
    totalSteps    <- max (data[which(data$seqId == seq),"stepIndex"])
    avgOrder      <- mean(data[which(data$seqId == seq),colName    ])
    
    tmpData <- data.frame(avgDuration = c(totalDuration/totalSteps),
                          avgOrder = avgOrder)

    plotData <- rbind(plotData, tmpData)
    
    print(paste(seq,paste(totalDuration, paste(totalSteps, avgOrder))))
}

print(plotData)

g <- ggplot(plotData, aes(x=avgOrder, y = avgDuration))
g <- g + geom_point()
if (walkFreq > 0) {
    g <- g + geom_hline(yintercept=1 /(2*walkFreq))
}
ggsave(paste0("step_duration_", paste0(colName, ".png")))
