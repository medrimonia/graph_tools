library(ggplot2)
library(stringr)

se <- function(x)
{
    sd(x) / (sqrt(length(x)))
}

ci <- function(x)
{
    1.96 * se(x)
}

plotParamsByLearningSize <- function(data)
{
    # Plot data is not equivalent to data
    plotData <- data.frame(param = character(),
                           value = double(),
                           ci = double(),
                           learningSize = integer(),
                           stringsAsFactors=FALSE)
    print(head(data))
    # For each parameter (format is hard coded)
    for (i in 5:length(names(data)))
    {
        colname <- names(data)[i]
        tmpData <- do.call(data.frame,aggregate(formula(paste0(colname,"~learningSize")),
                                                data,
                                                function(x) c(mean = mean(x),
                                                              ci = ci(x))))
        nrow <- nrow(tmpData)
        tmpData$param <- rep(colname, nrow)
        tmpData$value <- tmpData[,paste0(colname,".mean")]
        tmpData$ci <- tmpData[,paste0(colname,".ci")]
        tmpData <- tmpData[,c("param","value","ci","learningSize")]
        plotData <- rbind(plotData,tmpData)
    }
    print(head(plotData))
    g <- ggplot(plotData, aes_string(x="learningSize",
                                     y="value",
                                     ymin="value - ci",
                                     ymax="value + ci",
                                     group="param"))
    g <- g + geom_point()
    g <- g + geom_line()
    g <- g + geom_ribbon(alpha=0.3)
    g <- g + facet_wrap(~param, scales="free_y",nrow=3)
    ggsave("params.png",width=16,height=9)     
}

plotParams <- function(data)
{
    # Plot data is not equivalent to data
    plotData <- data.frame(param = character(),
                           value = double(),
                           ci = double(),
                           stringsAsFactors=FALSE)
    print(head(data))
    # For each parameter (format is hard coded)
    for (i in 5:length(names(data)))
    {
        colname <- names(data)[i]
        print(paste0("treating ", colname))
        tmpData <- data.frame(param = colname,
                              value = mean(data[,colname]),
                              ci = ci(data[,colname]),
                              stringsAsFactors=FALSE)
        print(paste0("treating ", colname))
        plotData <- rbind(plotData,tmpData)
    }
    print(plotData)
    g <- ggplot(plotData, aes_string(x="param",
                                     y="value",
                                     ymin="value - ci",
                                     ymax="value + ci"))
    g <- g + geom_point()
    g <- g + geom_errorbar()
    ggsave("params.png",width=16,height=9)     
}

args <- commandArgs(TRUE)

if (length(args) < 1) {
    cat("Usage: ... <runFile.csv>\n");
    quit(status=1)
}

path = args[1]

data <- read.csv(path)

plotParams(data)
