library(ggplot2)
library(stringr)

columnNames <- c("seq", "index",
                 "readPoseX", "readPoseY", "readPoseYaw", "readSupportFoot",
                 "goalPoseX", "goalPoseY", "goalPoseYaw", "goalSupportFoot",
                 "walkX", "walkY","walkTheta","walkEnabled", "walkPhase",
                 "targetX","targetY","targetA")

cbbPalette <- c("#000000", "#E69F00", "#56B4E9", "#009E73", "#F0E442", "#0072B2", "#D55E00", "#CC79A7")

plotDensity <- function(data, colName)
{
    # Determining bandwidth with respect to min and max
    diff = max(data[,colName]) - min(data[,colName])
    bandwidth= diff / 50
    # Plotting
    g <- ggplot(data, aes_string(x=colName))
    g <- g + geom_density(bw=bandwidth)
    ggsave(sprintf("%s_density.png", colName))    
}

analyzeLog <- function(path)
{
    # Read data file
    data <- read.csv(path, sep=" ", header=FALSE,
                     col.names = columnNames)
    # Filter to keep only first steps (approximative)
    data <- data[which(data$walkPhase < 0.01),]

    g <- ggplot(data, aes(x=walkX, y=walkY,color=walkTheta))
    g <- g + geom_point(size=0.5)
    g <- g + scale_color_gradientn(colors = cbbPalette[1:5])
    ggsave("walkOrders.png")

    for (colName in c("walkX","walkY","walkTheta")) {
        plotDensity(data,colName)
    }
}

args <- commandArgs(TRUE)

if (length(args) < 1) {
    cat("Usage: ... <runFile.csv>\n");
    quit(status=1)
}

path = args[1]

analyzeLog(path)
