library(ggplot2)
library(stringr)

# Plot the runFile produced by appOdometrySimulator in Leph::Model

gridWidth <- 3
gridLimits <- c(-gridWidth/2,gridWidth/2)

plotLog <- function(data, logId)
{
    logData <- data[which(data$log == logId),]
    trajectory <- logData[which(logData$type == "trajectory"),]
    samples <- logData[which(logData$type == "sample"),]
    observations <- logData[which(logData$type == "observation"),]
    # Plot cartesian view
    g <- ggplot()
    g <- g + geom_point(data=samples, mapping=aes(x=x,y=y),
                        size=0.2, color="black")
    g <- g + geom_point(data=observations, mapping=aes(x=x,y=y),
                        size=2, color="red")
    g <- g + geom_path(data=trajectory, mapping=aes(x=x,y=y,color=step))
    g <- g + coord_cartesian(xlim = gridLimits, ylim = gridLimits)
    ggsave(sprintf("run_%d.png",logId))
    # Plot angle distribution:
    # Trick: copy the data 3 times, 1: real value, 2: + 2*pi 3: -2*pi
    initialNbRows <- nrow(samples)
    samples <- rbind(samples,rbind(samples,samples))
    increasedIndices <- (initialNbRows+1):(2*initialNbRows)
    decreasedIndices <- (2*initialNbRows+1):(3*initialNbRows)
    samples[increasedIndices,"theta"] <- samples[increasedIndices,"theta"] + 2 * pi
    samples[decreasedIndices,"theta"] <- samples[decreasedIndices,"theta"] - 2 * pi
    samples[,"theta"] <- samples[,"theta"] * 180 / pi
    observations[,"theta"] <- observations[,"theta"] * 180 / pi
    g <- ggplot()
    g <- g + geom_density(data=samples, mapping=aes(x=theta),
                          size=0.5, color="black")
    g <- g + geom_vline(data=observations, mapping=aes(xintercept=theta),
                        size=2, color="red")
    g <- g + scale_x_continuous(breaks = c(-180, -90,0, 90, 180))
    g <- g + coord_cartesian(xlim = c(-180,180))
    ggsave(sprintf("angular_%d.png",logId))
}

args <- commandArgs(TRUE)

if (length(args) < 1) {
    cat("Usage: ... <runFile.csv>\n");
    quit(status=1)
}

path = args[1]

data <- read.csv(path)

# For each logs, plot it
for (logId in unique(data$log))
{
    plotLog(data, logId)
}
