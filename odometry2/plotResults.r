library(ggplot2)
library(stringr)

se <- function(x)
{
    sd(x) / (sqrt(length(x)))
}

ci <- function(x)
{
    1.96 * se(x)
}

plotByMethodAndLearningSize <- function(data)
{
    data$method <- paste(data$displacementModel,data$noiseModel);
    learnings <- do.call(data.frame, aggregate(learningScore~learningSize+method, data,
                                               function(x) c(mean = mean(x),
                                                             ci = ci(x))));
    tests <- do.call(data.frame, aggregate(testScore~learningSize+method, data,
                                           function(x) c(mean = mean(x),
                                                         ci = ci(x))));
    # Renaming columns
    learnings$value <- learnings[,"learningScore.mean"]
    learnings$ci    <- learnings["learningScore.ci"]
    tests$value     <- tests["testScore.mean"]
    tests$ci        <- tests["testScore.ci"]
    # plotting
    g <- ggplot()
    g <- g + geom_line(data =learnings,
                       size = 1.5,
                       mapping = aes(x =learningSize, y=value, group=method, color=method))
    g <- g + geom_ribbon(data =learnings,
                         mapping = aes_string(x="learningSize",
                                              ymin="value -ci",
                                              ymax="value+ci",
                                              fill="method",
                                              colour="method"),
                         alpha =0.1,
                         size=0.2)
    #TODO: different style
    g <- g + geom_line(data =tests,
                       size=1.5,
                       mapping = aes(x =learningSize, y=value, group=method, color=method))
    g <- g + coord_cartesian(ylim = c(-2,3))
    g <- g + geom_ribbon(data = tests,
                         mapping = aes_string(x="learningSize",
                                              ymin="value -ci",
                                              ymax="value+ci",
                                              fill="method",
                                              group="method",
                                              colour="method"),
                         alpha =0.1,
                         size=0.2)
    ggsave("results.png", width=16, height=9)
}

plotByMethod <- function(data) {
    data$method <- paste(data$displacementModel,data$noiseModel);
    learnings <- do.call(data.frame, aggregate(learningScore~method, data,
                                               function(x) c(mean = mean(x),
                                                             ci = ci(x))));
    tests <- do.call(data.frame, aggregate(testScore~method, data,
                                           function(x) c(mean = mean(x),
                                                         ci = ci(x))));
    # Renaming columns
    learnings$value <- learnings[,"learningScore.mean"]
    learnings$ci    <- learnings["learningScore.ci"]
    tests$value     <- tests["testScore.mean"]
    tests$ci        <- tests["testScore.ci"]
    # plotting learning performance
    g <- ggplot(data =learnings,
                mapping = aes_string(x="method", y="value",
                                     ymin="value-ci",ymax="value+ci"))
    g <- g + geom_point()
    g <- g + geom_errorbar(width=0.5)
    ggsave("learningResults.png", width=16, height=9)
    # plotting validation performance
    g <- ggplot(data =tests,
                mapping = aes_string(x="method", y="value",
                                     ymin="value-ci",ymax="value+ci"))
    g <- g + geom_point()
    g <- g + geom_errorbar(width=0.5)
    ggsave("validationResults.png", width=16, height=9)
}


args <- commandArgs(TRUE)

if (length(args) < 1) {
    cat("Usage: ... <runFile.csv>\n");
    quit(status=1)
}

path = args[1]

data <- read.csv(path)

#plotByMethodAndLearningSize(data)
plotByMethod(data)

warnings()
